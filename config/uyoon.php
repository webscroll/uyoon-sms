<?php

/**
 * Application Specific Configuration
 */

return [
    // Pagination Config
    'pagination' => ['perPage' => 20],

    'gender' => [
        'male' => 'Male',
        'female' => 'Female',
    ],

    // Project Options
    'programs' => [
        'status' => [
            'active' => 'Active',
            'inactive' => 'Inactive',
        ],
    ],
];
