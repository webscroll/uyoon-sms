/* ============================================================
 * Dashboard
 * Generates widgets in the dashboard
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
// line chart

(function () {
  'use strict';
  $(document).ready(function () {
    // Init portlets

    //var bars = $('.widget-loader-bar');
    var circles = $('.widget-loader-circle');
    //var circlesLg = $('.widget-loader-circle-lg');
    //var circlesLgMaster = $('.widget-loader-circle-lg-master');



    // bars.each(function () {
    //   var elem = $(this);
    //   elem.card({
    //     progress: 'bar',
    //     onRefresh: function () {
    //       setTimeout(function () {
    //         elem.card({
    //           refresh: false
    //         });
    //       }.bind(this), 2000);
    //     }
    //   });
    // });


    circles.each(function () {
      var elem = $(this);
      elem.card({
        progress: 'circle',
        onRefresh: function () {
          refreshEventList();

          setTimeout(function () {
            elem.card({
              refresh: false
            });
          }.bind(this), 2000);
        }
      });
    });

    // circlesLg.each(function () {
    //   var elem = $(this);
    //   elem.card({
    //     progress: 'circle-lg',
    //     progressColor: 'white',
    //     overlayColor: '0,0,0',
    //     overlayOpacity: 0.6,
    //     onRefresh: function () {
    //       setTimeout(function () {
    //         elem.card({
    //           refresh: false
    //         });
    //       }.bind(this), 2000);
    //     }
    //   });
    // });


    // circlesLgMaster.each(function () {
    //   var elem = $(this);
    //   elem.card({
    //     progress: 'circle-lg',
    //     progressColor: 'master',
    //     overlayOpacity: 0.6,
    //     onRefresh: function () {
    //       setTimeout(function () {
    //         elem.card({
    //           refresh: false
    //         });
    //       }.bind(this), 2000);
    //     }
    //   });
    // });
  });


  // Function to Refresh Event list
  function refreshEventList() {
    // Get Date
    var date = $("input[name='date']").val()

    $.ajax({
      url: `/api/events?date=${date}`,
      success: function (events) {

        // Clearn Div
        $(".event-list").empty();
        var markup = '';

        // Loop thought Events
        for (var event of events) {
          markup += `<div class="task clearfix row ${event.is_complete === 1 ? 'completed' : ''}">
                  <div class="task-list-title col-10 justify-content-between ">
                      <a href="#" class="text-master ${event.is_complete === 1 ? 'strikethrough' : ''}" data-task="name">${event.title}</a>
                      <i class="fs-14 pg-close hidden delete-event" data-id=${event.id}></i>
                  </div>
                  <div class="checkbox checkbox-circle no-margin text-center col-2 d-flex justify-content-center">
                      <input type="checkbox"  ${event.is_complete === 1 ? 'checked="chcked"' : ''} value="${event.is_complete}" id="event-id-${event.id}" data-toggler="task" class="hidden" data-id=${event.id} name="is_complete">
                      <label for="event-id-${event.id}" class=" no-margin no-padding absolute"></label>
                  </div>
              </div>`;
        }

        $(".event-list").append(markup);
      },
      error: function (error) {
        console.log(error);
      }
    });
  }

  // Populate Event List
  $(document).ready(function () {
    if ($("#create-event").length > 0) {
      refreshEventList();
    }
  })


  // Events - Submit Event
  $(document).on("submit", "#create-event", function (e) {
    e.preventDefault(); // Prevent Default form Submission
    $.ajax({
      type: "post",
      url: '/api/events',
      data: $("#create-event").serialize(),
      success: function (store) {
        console.log(store)
        // Reset Field
        $("#create-event").trigger("reset");

        // Refresh the list
        refreshEventList();
      },
      error: function (error) {
        console.log(error);
      }
    });
  });

  // Events - Status Update
  $(document).on("click", "input[name='is_complete']", function (e) {
    var id = $(this).data("id")

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "put",
      url: `/api/events/${id}`,
      data: { 'is_complete': $(this).attr("value") == '1' ? '0' : '1' },
      success: function (store) {
        // Refresh the list
        refreshEventList();
      },
      error: function (error) {
        console.log(error);
      }
    });
  });

    // Events - Delete Event
    $(document).on("click", ".delete-event", function (e) {
      var id = $(this).data("id")
      
      $.ajax({
        type: "DELETE",
        url: `/api/events/${id}`,
        success: function (store) {
          console.log(store);
          
          // Refresh the list
          refreshEventList();
        },
        error: function (error) {
          console.log(error);
        }
      });
    });

})();