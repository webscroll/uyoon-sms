(function ($) {

  'use strict';

  $(document).ready(function () {
    // Initializes search overlay plugin.
    // Replace onSearchSubmit() and onKeyEnter() with
    // your logic to perform a search and display results
    $(".list-view-wrapper").scrollbar();

    $('[data-pages="search"]').search({
      // Bind elements that are included inside search overlay
      searchField: '#overlay-search',
      closeButton: '.overlay-close',
      suggestions: '#overlay-suggestions',
      brand: '.brand',
      // Callback that will be run when you hit ENTER button on search box
      onSearchSubmit: function (searchString) {
        console.log("Search for: " + searchString);
      },
      // Callback that will be run whenever you enter a key into search box.
      // Perform any live search here.
      onKeyEnter: function (searchString) {
        console.log("Live search for: " + searchString);
        var searchField = $('#overlay-search');
        var searchResults = $('.search-results');

        /*
            Do AJAX call here to get search results
            and update DOM and use the following block
            'searchResults.find('.result-name').each(function() {...}'
            inside the AJAX callback to update the DOM
        */

        // Timeout is used for DEMO purpose only to simulate an AJAX call
        clearTimeout($.data(this, 'timer'));
        searchResults.fadeOut("fast"); // hide previously returned results until server returns new results
        var wait = setTimeout(function () {

          searchResults.find('.result-name').each(function () {
            if (searchField.val().length != 0) {
              $(this).html(searchField.val());
              searchResults.fadeIn("fast"); // reveal updated results
            }
          });
        }, 500);
        $(this).data('timer', wait);

      }
    })

  });


  $('.panel-collapse label').on('click', function (e) {
    e.stopPropagation();
  })

  // auto init for parallax sets window as scrollElement.
  // set .page-container as scrollElement for horizontal layouts.
  $('.jumbotron').parallax({
    scrollElement: '.page-container'
  })

  $('.page-container').on('scroll', function () {
    $('.jumbotron').parallax('animate');
  });

  /**
   * Display a confirmation alert for delete buttons
   */
  $('.delete-form').submit(function (e) {
    if (confirm("Are you sure you would like to conitnue this action?")) {
      return true;
    }

    return false;
  });

  $('.delete-onclick').click(function (e) {
    e.preventDefault();
    var $form = $(this);
    $('#confirm').modal({ backdrop: 'static', keyboard: true })
      .on('click', '#delete-btn', function () {
        $form.submit();
      });
  });

  //https://github.com/colebemis/feather
  //Feather ICONS
  //Used in sidebar icons
  if (feather) {
    feather.replace({
      'width': 16,
      'height': 16
    });
  }

  $(".hide-notification").fadeTo(2000, 500).fadeOut(500, function(){
    $(".hide-notification").fadeOut(500);
  });

})(window.jQuery);
