<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');

// Admin Routes
Route::namespace('Admin')->middleware('auth')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace

    // Program Routes
    Route::resource('programs', 'ProgramController');
    Route::resource('modules', 'ModuleController');

    // Nested Route for Programs & Modules
    Route::resource('programs.modules', 'ModuleProgramController');

    // Batch
    Route::resource('batches', 'BatchController');
    Route::resource('batches.students', 'BatchStudentController');

    Route::resource('students', 'StudentController');
    Route::resource('vendors', 'VendorController');
    Route::resource('customers', 'CustomerController');
    Route::resource('expenses', 'ExpenseController');
    Route::resource('incomes', 'IncomeController');

    Route::get('export', 'StudentController@export')->name('export');
    Route::post('import', 'StudentController@import')->name('import');




});
