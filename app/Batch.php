<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Batch extends Model
{
    // Mass Assignable Fields
    protected $fillable = [
        'program_id',
        'short_code',
        'status',
        'starts_at',
        'ends_at',
    ];

    // Date Senatize
    public function setStartsAtAttribute($value)
    {
        $this->attributes['starts_at'] =  Carbon::parse($value);
    }

    public function setEndsAtAttribute($value)
    {
        $this->attributes['ends_at'] =  Carbon::parse($value);
    }

    // Relationships
    public function program()
    {
        return $this->belongsTo('App\Program');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student')->withPivot('registration_status');
    }

    public function studentsCount()
    {
        return $this->belongsToMany('App\Student')
            ->selectRaw('count(students.id) as aggregate')
            ->groupBy('pivot_student_id');
    }
    
}
