<?php

namespace App\Exports;

use App\Student;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class StudentsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Student::all();
    }
    public function headings(): array
    {
        return [
            '#',
            'StudentID',
            'Name',
            'NationalID',
            'DOB',
            'Gender',
            'permanent_address',
            'current_address',
            'Mobile',
            'Email',
            'GurdianName',
            'GurdianMobile',
            'Nationality',
            'current_island',
            'permanent_island',
            'Qualification',
            'Created at',
            'Updated at'
        ];
    }
}
