<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

    // Mass Assignable Fields
    protected $fillable = [
        'user_id',
        'title',
        'is_complete'
    ];
}
