<?php

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Student([
                'student_id' => $row[0],
                'national_id' => $row[1],
                'name' => $row[2],
                'dob' => $row[3],
                'gender' => $row[4],
                'permanent_address' => $row[5],
                'current_address' => $row[6],
                'mobile_number' => $row[7],
                'email' => $row[8],
                'nationality' => $row[9],
                'current_island' => $row[9],
                'permanent_island' => $row[9],
                'guardian_name' => $row[10],
                'guardian_mobile_number' => $row[11],
        ]);
    }
}
