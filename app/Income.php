<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $guarded = [];

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

}
