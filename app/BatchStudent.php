<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchStudent extends Model
{
    // Set Default Value
    protected $attributes = [ 'attendance' => 0, 'module_ids' => [], 'registration_status' => 'not_registered'];

    // Mass Assignable Fields
    protected $fillable = [
        'attendance',
        'module_ids',
        'registration_status'
    ];
}
