<?php

namespace App\Http\Controllers\Admin;

use App\Batch;
use App\Program;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BatchRequest;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get list of Batchs
        $batches = Batch::when($request->input('q'), function ($query, $q) {
            return $query->where('short_code', 'LIKE', "%${q}%");
            })->orderBy('short_code', 'asc')->with('program')->paginate(14);

        // Get required params
        $programs = Program::where('status', 'active')->get();

        return view('admin.batches.index', compact('batches', 'programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BatchRequest $request)
    {
        // Store Batches
        Batch::create($request->all());

        // Redirect to index page
        return redirect()->back()->with('success', 'Batch Added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Batch $batch)
    {
        // Eager Load
        $batch->load('students', 'program', 'studentsCount');

        // Send All Modules
        $add_students = Student::whereDoesntHave('batches', function($q) use($batch)
        {
            $q->where('batch_id', $batch->id);
        })->get();
         
        return view('admin.batches.show', compact('batch', 'add_students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Batch $batch)
    {
        // Get required params
        $programs = Program::where('status', 'active')->get();

        // Return to view
        return view('admin.batches.edit', compact('batch', 'programs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batch $batch)
    {
        // Get Program
        $batch->update($request->all());

        // Redirect back to Index
        return redirect()->route('batches.index')->with('success', 'Batch Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Batch $batch)
    {
        // Delete Batch
        $batch->delete();
        return redirect()->route('batches.index')->with('danger', 'Batch Deleted successfully!');
    }
}
