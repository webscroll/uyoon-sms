<?php

namespace App\Http\Controllers\Admin;

use App\Student;
use App\ProgramLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PragmaRX\Countries\Package\Countries;
use App\Http\Requests\StudentRequest;
use Illuminate\Support\Facades\DB;
use App\Exports\StudentsExport;
use App\Imports\StudentsImport;
use Maatwebsite\Excel\Facades\Excel;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     // Get list of Students
     $students = Student::when($request->input('q'), function ($query, $q) {
        return $query->where('name', 'LIKE', "%${q}%")->orWhere('national_id', 'LIKE', "%${q}%")->orWhere('mobile_number', 'LIKE', "%${q}%")->orWhere('student_id', 'LIKE', "%${q}%");
        })->orderBy('id', 'asc')->paginate(config('uyoon.pagination.perPage'));

    // Return to view
    return view('admin.students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Countries::all()->pluck('name.common', 'name.common');
        // Get Levels
        $levels = ProgramLevel::all()->sortBy('sort_order')->groupBy('type');
        return view('admin.students.create', compact('countries', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        // Create Student
        Student::create($request->all()+['student_id' => '']);

        // Redirect to index page
        return redirect(route('students.index'))->with('success', 'Student Added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Student $student)
    {
        $student->Load(['batches.program', 'level']);
        return view('admin.students.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Student $student)
    {
        // Get Countries
        $countries = Countries::all()->pluck('name.common', 'name.common');
        // Get Levels
        $levels = ProgramLevel::all()->sortBy('sort_order')->groupBy('type');

        return view('admin.students.edit', compact('student', 'countries', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, Student $student)
    {
        // Get Program
        $student->update($request->all());

        // Redirect back to Index
        return redirect()->route('students.index')->with('success', 'Student Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        // Delete Request
        $student->delete();
        return redirect()->route('students.index')->with('danger', 'Student Deleted successfully!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new StudentsExport, 'allStudents.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new StudentsImport,request()->file('file'));

        return redirect()->route('students.index')->with('success', 'All students added!');
    }
}
