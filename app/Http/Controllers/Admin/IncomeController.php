<?php

namespace App\Http\Controllers\admin;

use App\Income;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeRequest;
use App\Customer;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $incomes = Income::when($request->input('q'), function ($query, $q) {
            return $query->where('invoice_number', 'LIKE', "%${q}%")->orWhere('title', 'LIKE', "%${q}%");
            })->orderBy('title', 'asc')->paginate(14);

        $customers = Customer::all();
        $incomes->load(['customer']);

        return view('admin.incomes.index', compact('incomes','customers'));
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncomeRequest $request, Income $income)
    {
        $user = Auth()->user()->name;
        $request->request->add(['received_by', ($user)]);

        $income = Income::create(request([
            'customer_id',
            'title',
            'type',
            'cheque_number',
            'description',
            'date',
            'received_by',
            'amount',
        ]));
        
        $income->invoice_number = 'INV'.date("y")."/".$income->id;
        $income->save();

        return redirect()->route('incomes.index')->with('success', 'Income Added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function show(Income $income)
    {
        $customers = Customer::all();

        $income->load(['customer']);

        return view('admin.incomes.show', compact('income','vendors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function edit(Income $income)
    {
        $customers = Customer::all();

        return view('admin.incomes.edit', compact ('income', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function update(IncomeRequest $request, Income $income)
    {
        // $user = Auth()->user()->name;
        // $request->request->add(['received_by', ($user)]);
        
        $income->fill(request([
            'customer_id',
            'title',
            'type',
            'cheque_number',
            'description',
            'date',
             ]));

        $income->save();
    
        return redirect()->route('incomes.index')->with('success', 'Expense Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function destroy(Income $income)
    {
        $income->delete();
        return redirect()->route('incomes.index')->with('danger', 'Income Deleted successfully!');;
    }
}
