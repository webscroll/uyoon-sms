<?php

namespace App\Http\Controllers\Admin;

use App\Program;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Program $program)
    {
        // Attach Module
        $program->modules()->attach($request->module_id);

        $module = Module::find($request->module_id);

        $program->update([
            'total_credit' => $program->total_credit+$module->credit,
        ]);

        // Redirect back
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Program $program, Module $module)
    {
        // Detach Module
        $program->modules()->detach($module->id);

        $program->update([
            'total_credit' => $program->total_credit-$module->credit,
        ]);

        // Redirect back
        return redirect()->back();
    }
}
