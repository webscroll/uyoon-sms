<?php

namespace App\Http\Controllers\Admin;

use App\Batch;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BatchStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Batch $batch)
    {
        // Attach Module
        $batch->students()->attach($request->student_id, ['attendance' => 1, 'registration_status' => 'not_registered', 'module_ids' => '[]' ]);

        // Redirect back
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Batch $batch, Student $student)
    {
        // Detach Student
        $batch->students()->detach($request->student_id);

        // Redirect to Bacth\
        return redirect()->back()->with('danger', 'Student Removed successfully!');;
    }
}
