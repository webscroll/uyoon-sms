<?php

namespace App\Http\Controllers\Admin;

use App\ProgramLevel;
use App\Program;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProgramRequest;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get list of Programs
        $programs = Program::when($request->input('q'), function ($query, $q) {
            return $query->where('short_code', 'LIKE', "%${q}%")->orWhere('name', 'LIKE', "%${q}%");
            })->orderBy('name', 'asc')->paginate(14);
        
        // Get Levels
        $levels = ProgramLevel::all()->sortBy('sort_order')->groupBy('type');
        
        // Return to view
        return view('admin.programs.index', compact('programs', 'levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramRequest $request)
    {   
        // Create Program
        Program::create($request->all());

        // Redirect to index page
        return redirect()->back()->with('success', 'Program Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        $program->load(['level', 'modules']);

        // Send All Modules
        $add_modules = Module::whereDoesntHave('programs', function($q) use($program)
        {
            $q->where('program_id', $program->id);
         })->get();

        return view('admin.programs.show', compact('program', 'add_modules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {   
        $levels = ProgramLevel::all()->sortBy('sort_order')->groupBy('type');
        return view('admin.programs.edit', compact('program', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramRequest $request, Program $program)
    {
        // Get Program
        $program->update($request->all());
        
        // Redirect back to Index
        return redirect()->route('programs.index')->with('success', 'Program Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
        $program->delete();
        return redirect()->route('programs.index')->with('danger', 'Program Deleted successfully!');
    }
}
