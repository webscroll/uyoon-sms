<?php

namespace App\Http\Controllers\Admin;

use App\Expense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExpenseRequest;
use App\Vendor;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $expenses = Expense::when($request->input('q'), function ($query, $q) {
            return $query->where('pv_number', 'LIKE', "%${q}%")->orWhere('title', 'LIKE', "%${q}%");
            })->orderBy('title', 'asc')->paginate(14);

        // $expenses = Expense::all();
        $vendors = Vendor::all();
        $expenses->load(['vendor']);

        // dd($expenses);

        return view('admin.expenses.index', compact('expenses','vendors'));
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $vendors = Vendor::all();
        // return view ('admin.expenses.create', compact('vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request, Expense $expense)
    {
        $user = Auth()->user()->name;
        $request->request->add(['paid_by', ($user)]);
        
        $expense = Expense::create(request([
            'vendor_id',
            'title',
            'type',
            'cheque_number',
            'description',
            'date',
            'amount',
            'paid_by',
            'receipt_number',
        ]));

        $expense->pv_number = 'PV'.date("y")."/".$expense->id;
        $expense->save();

        return redirect()->route('expenses.index')->with('success', 'Expense Added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {

        $vendors = Vendor::all();

        $expense->load(['vendor']);

        return view('admin.expenses.show', compact('expense','vendors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        $vendors = Vendor::all();
        //dd($vendors);
        return view('admin.expenses.edit', compact ('expense', 'vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, Expense $expense)
    {

        $user = Auth()->user()->name;
        $request->request->add(['paid_by', ($user)]);

        $expense->fill(request([
            'vendor_id',
            'title',
            'type',
            'cheque_number',
            'description',
            'date',
            'pv_number',
            'receipt_number',
             ]));

        $expense->save();
    
        return redirect()->route('expenses.index')->with('success', 'Expense Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
        return redirect()->route('expenses.index')->with('danger', 'Expense Deleted successfully!');;
    }
}
