<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Event;
use App\Income;
use App\Expense;
use App\Program;
use App\Student;

use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Count Batch, Student & program
        $batches_count = Batch::count();
        $students_count = Student::count();
        $programs_count = Program::count();

        // Query Todays Incomes
        $incomes = Income::whereDate('created_at', Carbon::today())->get();
        $incomes_count = Income::whereDate('created_at', Carbon::today())->sum('amount');

        // Query Todays Expenses
        $expenses = Expense::whereDate('created_at', Carbon::today())->get();
        $expenses_count = Expense::whereDate('created_at', Carbon::today())->sum('amount');

        $events = Event::whereDate('created_at', Carbon::today())->get();
        return view('admin.dashboard.index', compact('students_count', 'batches_count','programs_count', 'incomes', 'incomes_count', 'events', 'expenses', 'expenses_count'));
    }
}
