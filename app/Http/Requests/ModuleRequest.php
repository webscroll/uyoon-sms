<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|unique:modules|max:255',
                    'short_code' => 'required|unique:modules|max:255',
                    'credit' => 'required|integer',
                    'fees' => 'required|max:255',
                    'repeat_fees' => 'required|max:255',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'      => 'required|unique:modules,name,'.$this->module->id,
                    'short_code'      => 'required|unique:modules,short_code,'.$this->module->id,
                    'credit' => 'required|integer',
                    'fees' => 'required|max:255',
                    'repeat_fees' => 'required|max:255',
                ];
            }
            default:break;
        }
    }
}
