<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Expense;

class ExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'type' => 'required',
                    'date' => 'required',
                    'amount' => 'required',
                    'cheque_number' => 'max:255',
                    'receipt_number' => 'max:255',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'type' => 'required',
                    'date' => 'required',                    
                    'amount' => 'required',
                    'cheque_number' => 'max:255',
                    'receipt_number' => 'max:255',
                ];
            }
            default:break;
        }
    }
}
