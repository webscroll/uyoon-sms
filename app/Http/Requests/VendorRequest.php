<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Vendor;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|unique:vendors|max:255',
                    'email' => 'required|email|unique:vendors|max:255',
                    'address' => 'required',
                    'tin_number' => 'required',
                    'mobile_number' => 'required|numeric',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'      => 'required|unique:vendors,name,'.$this->vendor->id,
                    'tin_number'      => 'required|unique:vendors,tin_number,'.$this->vendor->id,
                    'email'      => 'required|email|unique:vendors,email,'.$this->vendor->id,
                    'address' => 'required',
                    'mobile_number' => 'required|numeric',
                ];
            }
            default:break;
        }
    }
}
