<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Program;

class ProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|unique:programs|max:255',
                    'short_code' => 'required|unique:programs|max:255',
                    'program_level_id' => 'required|integer',
                    'status' => 'required|max:255',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'short_code'      => 'required|unique:programs,short_code,'.$this->program->id,
                    'name'      => 'required|unique:programs,name,'.$this->program->id,
                    'program_level_id' => 'required|integer',
                    'status' => 'required|max:255',
                ];
            }
            default:break;
        }
    }
}
