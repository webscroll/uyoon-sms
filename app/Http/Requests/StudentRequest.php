<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Program;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|max:255',
                    'national_id' => 'required|unique:students|max:255',
                    'dob' => 'required',
                    'gender' => 'required|max:255',
                    'permanent_address' => 'required|max:255',
                    'current_address' => 'required|max:255',
                    'mobile_number' => 'required|max:255',
                    'nationality' => 'required|max:255',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'      => 'required|max:255',
                    'national_id'      => 'required|unique:students,national_id,'.$this->student->id,
                    'dob' => 'required',
                    'gender' => 'required|max:255',
                    'permanent_address' => 'required|max:255',
                    'current_address' => 'required|max:255',
                    'mobile_number' => 'required|max:255',
                    'nationality' => 'required|max:255',
                ];
            }
            default:break;
        }
    }
}
