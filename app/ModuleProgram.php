<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleProgram extends Model
{
    // Set Default Value
    protected $attributes = [ 'status' => 'Active'];
}
