<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{

    // Set Default Value
    protected $attributes = [ 'total_credit' => 0];

    // Mass Assignable Fields
    protected $fillable = [
        'program_level_id',
        'name',
        'short_code',
        'status',
        'total_credit'
    ];

    /* Relationships */
    public function level()
    {
        return $this->hasOne('App\ProgramLevel', 'id', 'program_level_id');
    }

    public function modules()
    {
        return $this->belongsToMany('App\Module');
    }
}
