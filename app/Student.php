<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    // Mass Assignable Fields
       protected $fillable = [
        'name',
        'national_id',
        'dob',
        'gender',
        'permanent_address',
        'current_address',
        'mobile_number',
        'email',
        'guardian_name',
        'guardian_mobile_number',
        'nationality',
        'highest_qualification',
        'student_id',
        'permanent_island',
        'current_island'
    ];

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] =  Carbon::parse($value);
    }

    // Seralize Name
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    // Seralize ID Number
    public function setNationalIdAttribute($value)
    {
        $this->attributes['national_id'] = strtoupper($value);
    }

    // Generate Student ID
    public function setStudentIdAttribute($value)
    {
        // Get latest Record ID
        $id = DB::select('SELECT id FROM students ORDER BY id DESC LIMIT 1');
        
        // Catch Error if no records is there
        try {
            $id = (int)$id[0]->id;
        } catch (\Throwable $err) {
            $id = 1000;
        }

        // Generate Year based on Current Date
        $date = new \Datetime();

        // Set Attribute
        $this->attributes['student_id'] = 'S' .$date->format('y') .$id++;
    }

    // Relationships
    public function batches()
    {
        return $this->belongsToMany('App\Batch');
    }

    // Levels
    public function level()
    {
        return $this->hasOne('App\ProgramLevel', 'id', 'highest_qualification');
    }
}
