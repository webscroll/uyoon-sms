<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    // Mass Assignable Fields
    protected $fillable = [
        'name',
        'short_code',
        'credit',
        'fees',
        'repeat_fees',
        'learning_hours',
        'contact_hours',
    ];

    /* Relationships */

    // Programs that has Modules
    public function programs()
    {
        return $this->belongsToMany('App\Program');
    }
}
