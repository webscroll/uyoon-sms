<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('incomes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('customer_id')->unsigned()->references('id')->on('customers');
			$table->string('title');
			$table->string('type');
			$table->string('cheque_number')->nullable();
			$table->string('description');
			$table->date('date');
			$table->string('invoice_number')->nullable();
			$table->string('received_by');
			$table->string('amount');
			$table->timestamps();
		});
		DB::update("ALTER TABLE incomes AUTO_INCREMENT = 1001;");
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('incomes');
	}
}