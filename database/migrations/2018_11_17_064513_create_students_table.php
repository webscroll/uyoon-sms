<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('students', function(Blueprint $table) {
			$table->increments('id');
			$table->string('student_id');
			$table->string('name');
			$table->string('national_id');
			$table->date('dob');
			$table->string('gender');
			$table->text('permanent_address');
			$table->text('current_address');
			$table->string('mobile_number');
			$table->string('email')->nullable();
			$table->string('guardian_name')->nullable();
			$table->string('guardian_mobile_number')->nullable();
			$table->string('nationality');
			$table->string('current_island');
			$table->string('permanent_island');
			$table->string('highest_qualification')->nullable();
			$table->timestamps();
		});

		//then set autoincrement to 1000
		DB::update("ALTER TABLE students AUTO_INCREMENT = 1000;");
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('students');
	}
}