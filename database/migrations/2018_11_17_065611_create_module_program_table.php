<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('module_program', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('program_id')->unsigned()->references('id')->on('programs');
			$table->integer('module_id')->unsigned()->references('id')->on('modules');
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('module_program');
	}
}