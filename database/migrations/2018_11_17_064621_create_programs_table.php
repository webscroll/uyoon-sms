<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('programs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('program_level_id')->unsigned()->references('id')->on('program_levels');
			$table->string('name');
			$table->string('short_code');
			$table->integer('total_credit');
			$table->string('status');
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('programs');

	}
}