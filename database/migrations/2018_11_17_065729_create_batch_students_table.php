<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('batch_student', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('batch_id')->unsigned()->references('id')->on('batches');
			$table->integer('student_id')->unsigned()->references('id')->on('students');
			$table->integer('attendance');
			$table->string('module_ids');
			$table->string('registration_status');
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('batch_students');
	}
}