<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('c_id')->nullable();
			$table->string('name');
			$table->text('address');
			$table->string('email');
			$table->string('mobile_number');
			$table->timestamps();
		});
		DB::update("ALTER TABLE customers AUTO_INCREMENT = 1001;");
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('customers');
	}
}