<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('fees', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('student_id')->unsigned()->references('id')->on('students');
			$table->string('type');
			$table->integer('batch_id');
			$table->integer('module_id')->nullable();
			$table->float('total_amount');
			$table->float('balance');
			$table->integer('discount_amount')->nullable();
			$table->string('description')->nullable();
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('fees');
	}
}