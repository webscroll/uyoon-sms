<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('payouts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->boolean('is_paid');
			$table->float('amount');
			$table->datetime('date');
			$table->string('description');
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('payouts');
	}
}