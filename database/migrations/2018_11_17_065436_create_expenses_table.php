<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('expenses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('vendor_id')->unsigned()->references('id')->on('vendors');
			$table->string('title');
			$table->string('type');
			$table->string('cheque_number')->nullable();
			$table->string('description');
			$table->date('date');
			$table->string('paid_by')->nullable();
			$table->string('pv_number')->nullable();
			$table->string('amount');
			$table->string('receipt_number')->nullable();
			$table->timestamps();
		});
		DB::update("ALTER TABLE expenses AUTO_INCREMENT = 1001;");

	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		Schema::drop('expenses');
	}
}