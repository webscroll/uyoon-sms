<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassLecturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('lecturer_classes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('lecturer_id')->unsigned()->references('id')->on('lecturers');
			$table->integer('module_id')->unsigned()->references('id')->on('modules');
			$table->integer('batch_student_id')->unsigned()->references('id')->on('batch_students');
			$table->float('amount_per_hour');
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('lecturer_classes');
	}
}