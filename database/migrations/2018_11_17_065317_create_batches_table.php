<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('batches', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('program_id')->unsigned()->references('id')->on('programs');
			$table->string('short_code');
			$table->date('starts_at');
			$table->date('ends_at')->nullable();
			$table->string('status');
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down()
	{
		Schema::drop('batches');
	}
}