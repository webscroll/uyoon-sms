<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate random seed users
        $users = factory('App\User', 3)->create();

        DB::table('users')->insert([
            'username' => 'admin',
            'name' => 'Admin User',
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
            'type' => 'Admin',
        ]);
    }
}
