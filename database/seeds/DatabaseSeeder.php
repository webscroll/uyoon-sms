<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(VendorTableSeeder::class);
        $this->call(ProgramLevelTableSeeder::class);
    }
}
