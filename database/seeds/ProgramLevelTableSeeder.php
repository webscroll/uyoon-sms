<?php

use App\ProgramLevel;
use Illuminate\Database\Seeder;

class ProgramLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProgramLevel::create([
            "name" => 'Not Stated',
            "type" => '',
            "sort_order" => '1'
        ]);

        ProgramLevel::create([
            "name" => 'Certificate I',
            "type" => 'Certificate',
            "sort_order" => '1'
        ]);

        ProgramLevel::create([
            "name" =>  'Certificate II',
            "type" => 'Certificate',
            "sort_order" =>  '2'
        ]);

        ProgramLevel::create([
            "name" => 'Certificate III',
            "type" => 'Certificate',
            "sort_order" =>  '3'
        ]);

        ProgramLevel::create([
            "name" => 'Advanced Certificate',
            "type" => 'Certificate',
            "sort_order" =>  '4'
        ]);

        ProgramLevel::create([
            "name" => 'Diploma',
            "type" => 'Undergraduate',
            "sort_order" =>  '5'
        ]);

        ProgramLevel::create([
            "name" => 'Advanced Diploma',
            "type" => 'Undergraduate',
            "sort_order" =>  '6'
        ]);

        ProgramLevel::create([
            "name" => 'Bachelor‟s Degree',
            "type" => 'Undergraduate',
            "sort_order" =>  '7'
        ]);

        ProgramLevel::create([
            "name" => 'Post Graduate Diploma',
            "type" => 'Postgraduate',
            "sort_order" =>  '8'
        ]);

        ProgramLevel::create([
            "name" => "Master's Degree",
            "type" => 'Postgraduate',
            "sort_order" =>  '9'
        ]);

        ProgramLevel::create([
            "name" => 'Doctoral degree',
            "type" => 'Doctorate',
            "sort_order" =>  '10'
        ]);
    }
}
