<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'student_id' => 'S'.mt_rand(140000, 190000),
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'national_id' => 'A'.mt_rand(100000, 900000),
        'dob' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y-m-d'),
        'gender' => $faker->randomElement(['Male', 'Female']),
        'permanent_address' => $faker->address,
        'current_address' => $faker->address, // secret
        'mobile_number' => mt_rand(7000000, 7900000),
        'nationality' => $faker->country,
        'current_island' => "Male",
        'permanent_island' => "kaashidhoo",
    ];
});
