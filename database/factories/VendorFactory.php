<?php

use Faker\Generator as Faker;

$factory->define(App\Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->safeEmail,
        'address' => $faker->address,
        'tin_number' => "GST".mt_rand(1000, 2000),
        'mobile_number' => mt_rand(7000000, 7900000),
    ];
});
