@if (!count($data))
<div class="content mt-5">
    <div class="text-center">
        <img src="/img/empty.svg" class="img-fluid" alt="Responsive image">
    </div>
    <div class="text-center mt-3">
        {{ request()->input('q') ? "Your search did not match any $model." : "You don't have any $model yet." }}
    </div>
    <div class="text-center mt-2">
        @if (request()->input('q'))
            <a class="btn btn-primary btn-cons" href="{{ route($route) }}">Clear Search</a>
        @else
            {{ $slot }}
        @endif
    </div>
</div>
@endif