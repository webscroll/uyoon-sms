<!-- START HEADER -->
<div class="header">
  <div class="container">
    <div class="header-inner  pt-3">
      <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="horizontal-menu">
      </a>
      
      <div class="header-inner justify-content-start title-bar">
        <div class="brand inline align-self-end">
            <img src="/img/UyoonLogoWhiteCircle.svg" alt="logo" data-src="/img/UyoonLogoWhiteCircle.svg" data-src-retina="/img/UyoonLogoWhiteCircle.svg" width="35" height="35">
          </div>
          <h2 class="page-title align-self-end pb-2">
            @yield('title')
          </h2>
      </div>
      <div class="d-flex align-items-center">
        <!-- START User Info-->
        <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
          <span class="semi-bold">{{ auth()->user()->username }}</span>
        </div>
        <div class="dropdown pull-right sm-m-r-5">
          <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <span class="thumbnail-wrapper d32 circular inline">
              <img src="/img/profiles/user.png" alt="" data-src="/img/profiles/user.png" data-src-retina="/img/profiles/user.png"
                width="52" height="52">
            </span>
          </button>
          <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
            <a href="#" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
            <a href="#" class="dropdown-item"><i class="pg-outdent"></i> Feedback</a>
            <a href="#" class="dropdown-item"><i class="pg-signals"></i> Help</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            <a href="{{ route('logout') }}" class="clearfix bg-master-lighter dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <span class="pull-left">Logout</span>
              <span class="pull-right"><i class="pg-power"></i></span>
            </a>
          </div>
        </div>
        <!-- END User Info-->
      </div>
      
    </div>
    <!-- <div class="header-inner justify-content-start title-bar">

    </div> -->
    @include('shared.header')
  </div>
</div>
