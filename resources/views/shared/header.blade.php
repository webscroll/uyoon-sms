<div class="menu-bar header-sm-height" data-pages-init="horizontal-menu" data-hide-extra-li="4">
  <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
  </a>
  <ul>
    <li class=" active">
      <a href="{{ route('dashboard') }}">Dashboard</a>
    </li>
    <li>
      <a href="{{ route('batches.index') }}"><span class="title">Batches</span></a>
    </li>
    <li>
      <a href="{{ route('students.index') }}"><span class="title">Students</span></a>
    </li>
    <li>
      <a href="{{ route('programs.index') }}"><span class="title">Programs</span></a>
    </li>
    <li>
      <a href="{{ route('modules.index') }}"><span class="title">Modules</span></a>
    </li>
    <li>
        <a href="{{ route('incomes.index') }}"><span class="title">Income</span></a>
      </li>
      <li>
        <a href="{{ route('expenses.index') }}"><span class="title">Expenses</span></a>
      </li>
    <li>
      <a href="{{ route('vendors.index') }}"><span class="title">Vendors</span></a>
    </li>
    <li>
      <a href="{{ route('customers.index') }}"><span class="title">Customers</span></a>
    </li>

  </ul>
</div>
