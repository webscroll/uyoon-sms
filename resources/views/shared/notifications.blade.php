@if ($message = Session::get('success'))
<div class="pgn-wrapper hide-notification" data-position="top-right" style="top: 121px; right: 9.8%;">
    <div class="pgn push-on-sidebar-open pgn-simple">
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <span aria-hidden="true">×</span>
        <span class="sr-only">Close</span>
      </button>{{ $message }}</div>
    </div>
  </div>
@endif

@if ($message = Session::get('error'))
<div class="pgn-wrapper hide-notification" data-position="top-right" style="top: 121px; right: 9.8%;">
    <div class="pgn push-on-sidebar-open pgn-simple">
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">
          <span aria-hidden="true">×</span>
        <span class="sr-only">Close</span>
      </button>{{ $message }}</div>
    </div>
  </div>
@endif

@if ($message = Session::get('danger'))
<div class="pgn-wrapper hide-notification" data-position="top-right" style="top: 121px; right: 9.8%;">
    <div class="pgn push-on-sidebar-open pgn-simple">
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">
          <span aria-hidden="true">×</span>
        <span class="sr-only">Close</span>
      </button>{{ $message }}</div>
    </div>
  </div>
@endif

@if ($message = Session::get('info'))
<div class="pgn-wrapper hide-notification" data-position="top-right" style="top: 121px; right: 9.8%;">
    <div class="pgn push-on-sidebar-open pgn-simple">
      <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">
          <span aria-hidden="true">×</span>
        <span class="sr-only">Close</span>
      </button>{{ $message }}</div>
    </div>
  </div>
@endif