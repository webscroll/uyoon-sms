<div class="modal fade stick-up" id="confirm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <div class="modal-content">
        <div class="modal-header clearfix text-left">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
          <h5>Confirm Delete</h5>
        </div>
        <div class="modal-body">
          <p class="no-margin">Are you sure you would like to delete this record?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-cons  pull-left inline" data-dismiss="modal" id="delete-btn">Delete</button>
          <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>