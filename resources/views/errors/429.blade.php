@extends('layouts/admin')

@section('code', '429')
@section('title', __('Too Many Requests'))

@section('content-wrapper')
    <!-- START PAGE CONTENT -->
    <!-- START CONTAINER FLUID -->
    <div class="content p-3 lock-container full-height full-width">
        <div class="full-height align-items-center d-md-flex">
            <div class="row full-width align-items-center">
                <div class="col-md-6 col-md-mr-5">
                    <div style="background-image: url({{ asset('/img/illustrations/403.svg') }});" class="error-illustration"></div>
                </div>
                <div class="col-md-4 ">
                    <h1 class="error-number">404</h1>
                    <p class="p-b-10">Sorry, you are making too many requests to our servers</p>
                    <a class="btn btn-primary btn-cons" href="{{ route('dashboard') }}">Back to Dashboard</a>
                </div>
            </div>
        </div>
    </div>
    </div>

    @include('shared.copyright')
<!-- END CONTAINER FLUID -->
@endsection