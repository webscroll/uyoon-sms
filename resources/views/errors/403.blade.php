@extends('layouts/admin')

@section('code', '403')
@section('title', __('Forbidden'))

@section('content-wrapper')
    <!-- START PAGE CONTENT -->
    <!-- START CONTAINER FLUID -->
    <div class="content p-3 lock-container full-height full-width">
        <div class="full-height align-items-center d-md-flex">
            <div class="row full-width align-items-center">
                <div class="col-md-6 col-md-mr-5">
                    <div style="background-image: url({{ asset('/img/illustrations/403.svg') }});" class="error-illustration"></div>
                </div>
                <div class="col-md-4 ">
                    <h1 class="error-number">403</h1>
                    <p class="p-b-10">{{ $exception->getMessage() ?: 'Sorry, you are forbidden from accessing this page' }}</p>
                    <a class="btn btn-primary btn-cons" href="{{ route('dashboard') }}">Back to Dashboard</a>
                </div>
            </div>
        </div>
    </div>
    </div>

    @include('shared.copyright')
<!-- END CONTAINER FLUID -->
@endsection