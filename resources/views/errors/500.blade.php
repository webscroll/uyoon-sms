@extends('layouts/admin')

@section('code', '500')
@section('title', __('Error'))

@section('content-wrapper')
    <!-- START PAGE CONTENT -->
    <!-- START CONTAINER FLUID -->
    <div class="content p-3 lock-container full-height full-width">
        <div class="full-height align-items-center d-md-flex">
            <div class="row full-width align-items-center">
                <div class="col-md-6 col-md-mr-5">
                    <div style="background-image: url({{ asset('/img/illustrations/500.svg') }});" class="error-illustration"></div>
                </div>
                <div class="col-md-4 ">
                    <h1 class="error-number">500</h1>
                    <p class="p-b-10">Sorry, the server is reporting an error</p>
                    <a class="btn btn-primary btn-cons" href="{{ route('dashboard') }}">Back to Dashboard</a>
                </div>
            </div>
        </div>
    </div>
    </div>

    @include('shared.copyright')
<!-- END CONTAINER FLUID -->
@endsection