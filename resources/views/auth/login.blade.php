@extends('layouts.admin') 
@section('content-wrapper')

<div class="register-container full-height sm-p-t-30">
    <div class="d-flex justify-content-center flex-column full-height ">
        <div class="row full-width">
            <div class="col-md-5">
                <div class="d-flex justify-content-start align-items-center ">
                    <div class="">
                        <div class="thumbnail-wrapper m-r-10 m-t-50 border-left-white ">
                            <img width="50" height="50" data-src-retina="/img/UyoonLogo-01.svg" data-src="/img/UyoonLogo-01.svg" alt="" src="/img/UyoonLogo-01.svg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <p class="p-t-35">Sign into your account</p>
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <button class="close" data-dismiss="alert"></button> {{ implode('', $errors->all(':message')) }}
                    </div>
                @endif
                <!-- START Login Form -->
                <form id="form-login" class="p-t-15" role="form" novalidate="novalidate" action="{{ route('login') }}" method="POST">
                    @csrf 
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>Email</label>
                        <div class="controls">
                            <input type="text" name="email" placeholder="E-Mail" class="form-control" required="" aria-required="true" value="{{ old('email') }}">
                        </div>
                    </div>
                    <!-- END Form Control-->
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>Password</label>
                        <div class="controls">
                            <input type="password" class="form-control" name="password" placeholder="Credentials" required="" aria-required="true">
                        </div>
                    </div>

                    <!-- START Form Control-->
                    <div>
                        <!-- END Form Control-->
                        <div>
                            <button class="btn btn-primary btn-cons m-t-10 " type="submit">Sign in</button>
                        </div>
                </form>
                </div>
            </div>
            {{-- <img class="logo" src="/img/UyoonLogo-01.svg" alt="Uyoon Institute" data-src="/img/UyoonLogo-01.svg" data-src-retina="/img/UyoonLogo-01.svg"
                width="100" height="100"> --}}
        </div>
    </div>
@endsection