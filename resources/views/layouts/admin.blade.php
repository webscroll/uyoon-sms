<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Uyoon Institute - Student Managment System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- STYLES -->
    <link href="/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/plugins/pace/pace-theme-flash.css" rel="stylesheet" />
    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
    <link href="/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="/pages/css/pages-icons.css" rel="stylesheet" />
    <link href="/pages/css/pages.css" rel="stylesheet" />
    <link href="/css/dashboard.widgets.css" rel="stylesheet" />

    <link href="/css/style.css" rel="stylesheet" />
    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
    <link type="text/css" rel="stylesheet" href="/plugins/dropzone/css/dropzone.css">


    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="/pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header horizontal-menu horizontal-app-menu dashboard">
    @auth
      @include('shared.navbar')
    @endauth

    <div class="page-container">
      @section('content-wrapper')
        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper">
          <!-- START PAGE CONTENT -->
          <div class="content">
            @yield('content')
            @include('shared.copyright')
          </div>
        </div>
      @show
    </div>

    @yield('name')
    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    @include('shared.scripts')
    @include('shared.confirm')
    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
  </body>
</html>
