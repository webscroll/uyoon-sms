@extends('layouts.admin')

@section('title', 'Vendor')
@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
      <div class="col-lg-4 pull-right">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('vendors.index') }}">Vendors</a></li>
          <li class="breadcrumb-item active">Edit Vendor</li>
        </ol>
      </div>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="no-padding container-fixed-lg bg-white">
      <div class="d-flex justify-content-center ">
          <div class="container card card-default col-10 mt-3">
              <!-- START card -->
                <div class="card-header ">
                  <h5>Edit Vendor</h5>
                  <div class="pull-right">
                    <div class="col-xs-12">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="card-block">
                  {{ Form::model($vendor, ['route' => ['vendors.update', $vendor],'novalidate' => 'novalidate', 'role' => 'form', 'id' => 'form-personal' ]) }}
                  {{ method_field('PATCH') }}
                      @include('admin.vendors.form')
                  {{ Form::close() }}
                </div>
              <!-- END card -->
            </div>
      </div>


    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection