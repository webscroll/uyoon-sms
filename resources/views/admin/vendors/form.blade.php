  <div class="row clearfix">
    <div class="col-md-6">
      <div class="form-group form-group-default required {{ $errors->has('name') ? 'has-error' :'' }}">
        {{ Form::label('Vendor Name') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Vendor Name', 'required',]) }}
      </div>
      {!! $errors->first('name', '<label class="error" for="name">:message</label>') !!}
    </div>
    <div class="col-md-6 clearfix">
      <div class="form-group form-group-default required {{ $errors->has('email') ? 'has-error' :'' }}">
        {{ Form::label('Email') }}
        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required',]) }}
      </div>
      {!! $errors->first('email', '<label class="error" for="name">:message</label>') !!}
    </div>
  </div>

  <div class="row clearfix">
    <div class="col-md-8">
      <div class="form-group form-group-default required {{ $errors->has('address') ? 'has-error' :'' }}">
        {{ Form::label('Address') }}
        {{ Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address', 'required',]) }}
      </div>
      {!! $errors->first('address', '<label class="error" for="name">:message</label>') !!}
    </div>
    <div class="col-md-4">
      <div class="form-group form-group-default required {{ $errors->has('mobile_number') ? 'has-error' :'' }}">
        {{ Form::label('Mobile Number') }}
        {{ Form::text('mobile_number', null, ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'required',]) }}
      </div>
      {!! $errors->first('mobile_number', '<label class="error" for="name">:message</label>') !!}
    </div>
  </div> 
  <div class="row clearfix">    
    <div class="col-md-12">
      <div class="form-group form-group-default required {{ $errors->has('tin_number') ? 'has-error' :'' }}">
        {{ Form::label('TIN Number') }}
        {{ Form::text('tin_number', null, ['class' => 'form-control', 'placeholder' => 'TIN Number', 'required',]) }}
      </div>
      {!! $errors->first('tin_number', '<label class="error" for="name">:message</label>') !!}
    </div>
  </div> 
  
  <hr>
  <div class="row">
    <div class="col-md-8">
    </div>
    <div class="col-md-4 m-t-10 sm-m-t-10">
      {{ Form::submit(isset($vendor) ? 'Update Vendor' : 'Add Vendor', ['class' => 'btn btn-primary btn-block m-t-5']) }}
    </div>
  </div>
