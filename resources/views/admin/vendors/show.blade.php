@extends('layouts.admin')
@section('title', 'Vendors')
@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
      <div class="col-lg-4 pull-right">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('vendors.index') }}">Vendors</a></li>
          <li class="breadcrumb-item active">Vendor Details</li>
        </ol>
      </div>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding container-fixed-lg bg-white">
      <div class="container card card-default">
        <!-- START card -->
        <div class="card card-transparent">
          <div class="card-header ">
              <div class="row">
                  <div class="col-md-8">
                      <h4>Vendor Details</h4>
                  </div>
                  <div class="col-md-4">
                      <div class="pull-right">
                          <h5 class="semi-bold" >TIN: {{ $vendor->tin_number}}</h5>                        
                        </div>
                  </div>
                </div>
          </div>
          <div class="card-block">
              <!-- START card -->
              <div class="card card-transparent">
                <div class="card-block">
                  <div class="row">
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Vendor Name</h5>
                      <h6 class="hint-text">{{ $vendor->name}}</h5>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Address</h5>
                      <h6 class="hint-text">{{ $vendor->address }}</h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Email</h5>
                      <h6 class="hint-text">{{ $vendor->email}} </h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Contact Number</h5>
                      <h6 class="hint-text">{{ $vendor->mobile_number }}</h6>
                    </div>        
                  </div>
                </div>
          </div>
        </div>
        <!-- END card -->
      </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection