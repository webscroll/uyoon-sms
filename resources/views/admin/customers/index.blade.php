@extends('layouts.admin')

@section('title', 'Customers')

@section('content')

@include('admin.customers.create')
@include('shared.notifications')

  <!-- START PAGE CONTENT -->
  <div class="content ">
    <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
        <li class="breadcrumb-item active">Customers</li>
      </ol>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding    container-fixed-lg bg-white">
      <div class="container card card-default">
        <!-- START card -->
        <div class="card card-transparent">
          <div class="card-header ">
            <a class="btn btn-primary btn-cons" href="#" data-target="#create-customer" data-toggle="modal"></i> Add Customer</a>
            <div class="pull-right">
              <div class="col-xs-12">
                <form action="{{ route('customers.index') }}">
                    <div class="input-group transparent center-margin">
                        <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="{{ request()->input('q') }}">
                        <span class="input-group-addon ">
                            <i class="pg-search"></i>
                        </span>
                      </div>
                </form>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-block">
            
            <div class="table-responsive">
                <div  class="dataTables_wrapper no-footer">
                    <table class="table table-hover table-condensed dataTable no-footer" role="grid">
                  <thead>
                    <tr role="row">
                      <th style="width: 15%">Customer ID</th>
                      <th style="width: 25%">Name</th>
                      <th style="width: 20%">Address</th>
                      <th style="width: 20%">Email</th>
                      <th style="width: 10%">Mobile</th>
                      <th style="width: 20%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($customers as $customer)
                      <tr>
                        <td class="v-align-middle">{{ $customer->c_id }}</td>
                        <td class="v-align-middle semi-bold"><a href="{{ route('customers.show', $customer) }}">{{ $customer->name }}</a></td>
                        <td class="v-align-middle">{{ $customer->address }}</td>
                        <td class="v-align-middle semi-bold">{{ $customer->email }}</td>
                        <td class="v-align-middle semi-bold">{{ $customer->mobile_number }}</td>
                        <td class="v-align-middle semi-bold">
                            <a href="{{ route('customers.edit', $customer) }}">Edit</a>
                            <div class="vertical-line"></div>
                            <form action="{{ route('customers.destroy', ['id' => $customer->id ]) }}?_method=DELETE" class="delete-onclick inline" method="POST">
                                @csrf
                                <button class="btn text-danger btn-link m-0 p-0" type="submit">Delete</button>
                                </button>
                              </form>
                            </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

          {{-- Default Empty State --}}
            @component('shared.empty', ['route' => 'customers.index', 'model' => 'customers', 'data' => $customers])
              {{-- Create new Bttn --}}
              <a class="btn btn-primary btn-cons" href="#" data-target="#create-customer" data-toggle="modal">Add Customer</a>
            @endcomponent

            <div class="mt-3">
                {{ $customers->links() }}
            </div>
          </div>
        </div>
        <!-- END card -->
      </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection