<p class="m-t-10">Basic Information</p>
<div class="row">
  <div class="col-md-6">
    <div class="form-group form-group-default required {{ $errors->has('national_id') ? 'has-error' :'' }}">
      {{ Form::label('National ID') }}
      {{ Form::text('national_id', null, ['class' => 'form-control', 'placeholder' => 'National ID', 'required',]) }}
    </div>
    {!! $errors->first('national_id', '<label class="error" for="national_id">:message</label>') !!}
  </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="form-group form-group-default required {{ $errors->has('name') ? 'has-error' :'' }}">
        {{ Form::label('Name') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required',]) }}
      </div>
      {!! $errors->first('name', '<label class="error" for="name">:message</label>') !!}
    </div>
  </div>


<div class="row clearfix">
  <div class="col-md-6">
    <div class="form-group form-group-default required {{ $errors->has('dob') ? 'has-error' :'' }}">
      {{ Form::label('Date of Birth') }}
      {{ Form::text('dob', null, ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'required', 'id' => 'datepicker-component', 'autocomplete' => 'off', 'type' => 'date']) }}
    </div>
    {!! $errors->first('dob', '<label class="error" for="dob">:message</label>') !!}
  </div>

  <div class="col-md-6">
      <div class="form-group form-group-default form-group-default-select2 {{ $errors->has('gender') ? 'has-error' :'' }}">
          {{ Form::label('Gender') }}
          {{ Form::select('gender', config('uyoon.gender'), null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'data-disable-search' => 'true']) }}
      </div>
     {!! $errors->first('gender', '<label class="error" for="gender">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-group-default form-group-default-select2 {{ $errors->has('nationality') ? 'has-error' :'' }}">
            {{ Form::label('Nationality') }}
            {{ Form::select('nationality', $countries, isset($student) ? null : 'Maldives',  ['class' => 'full-width', 'data-init-plugin' => 'select2']) }}
        </div>
        {!! $errors->first('nationality', '<label class="error" for="nationality">:message</label>') !!}
    </div>

    <div class="col-md-6">
        <div class="form-group form-group-default form-group-default-select2 {{ $errors->has('highest_qualification') ? 'has-error' :'' }}">
            {{ Form::label('Highest Qualification') }}
              <select class="full-width" data-placeholder="Select Level" data-init-plugin="select2" name="highest_qualification">
                @foreach ($levels as $type => $levelGrouped)
                  <optgroup label="{{ $type }}">
                    @foreach ($levelGrouped as $level)
                      <option value="{{ $level->id }}" {{ isset($program) ? ($program->program_level_id == $level->id ? 'selected' : '') : '' }}>{{ $level->name }}</option>
                    @endforeach
                  </optgroup>
                @endforeach
              </select>
          </div>
      {!! $errors->first('highest_qualification', '<label class="error" for="highest_qualification">:message</label>') !!}
    </div>

  </div>
  
<p class="m-t-10">Contact Information</p>

<div class="row clearfix">
  <div class="col-md-6">
    <div class="form-group form-group-default required {{ $errors->has('current_address') ? 'has-error' :'' }}">
      {{ Form::label('Current Address') }}
      {{ Form::text('current_address', null, ['class' => 'form-control', 'placeholder' => 'Current Address', 'required']) }}
      </div>
    {!! $errors->first('current_address', '<label class="error" for="current_address">:message</label>') !!}
  </div>
  <div class="col-md-6">
      <div class="form-group form-group-default required form-group-default-select2 {{ $errors->has('current_island') ? 'has-error' :'' }}">
          {{ Form::label('Island') }}
          {{ Form::select('current_island', config('islands'), null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'placeholder' => 'Select a Island...', 'required']) }}
      </div>
     {!! $errors->first('current_island', '<label class="error" for="gender">:current_island</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-6">
      <div class="form-group form-group-default required {{ $errors->has('permanent_address') ? 'has-error' :'' }}">
        {{ Form::label('Permanent Address') }} {{ Form::text('permanent_address', null, ['class' => 'form-control', 'placeholder' => 'Permanent Address', 'required']) }}
      </div>
      {!! $errors->first('permanent_address', '<label class="error" for="permanent_address">:message</label>') !!}
    </div>
    <div class="col-md-6">
      <div class="form-group form-group-default form-group-default-select2 required {{ $errors->has('permanent_island') ? 'has-error' :'' }}">
        {{ Form::label('permanent_island') }} 
        {{ Form::select('permanent_island', config('islands'), null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'placeholder' => 'Select a Island...', 'required']) }}
      </div>
      {!! $errors->first('permanent_island', '<label class="error" for="gender">:permanent_island</label>') !!}
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-group-default required {{ $errors->has('mobile_number') ? 'has-error' :'' }}">
            {{ Form::label('Mobile Number') }}
            {{ Form::number('mobile_number', null, ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'required']) }}
        </div>
        {!! $errors->first('mobile_number', '<label class="error" for="mobile_number">:message</label>') !!}
    </div>
  
    <div class="col-md-6">
        <div class="form-group form-group-default {{ $errors->has('email') ? 'has-error' :'' }}">
            {{ Form::label('Email') }}
            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) }}
        </div>
        {!! $errors->first('email', '<label class="error" for="email">:message</label>') !!}
    </div>
</div>

<p class="m-t-10">Guardian Information</p>

<div class="row clearfix">
    <div class="col-md-7">
        <div class="form-group form-group-default {{ $errors->has('guardian_name') ? 'has-error' :'' }}">
            {{ Form::label('Guardian Name') }}
            {{ Form::text('guardian_name', null, ['class' => 'form-control', 'placeholder' => 'Current Address']) }}
            </div>
            {!! $errors->first('guardian_name', '<label class="error" for="guardian_name">:message</label>') !!}
    </div>

    <div class="col-md-5">
        <div class="form-group form-group-default {{ $errors->has('guardian_mobile_number') ? 'has-error' :'' }}">
            {{ Form::label('Mobile Number') }}
            {{ Form::number('guardian_mobile_number', null, ['class' => 'form-control', 'placeholder' => 'Mobile Number']) }}
        </div>
        {!! $errors->first('guardian_mobile_number', '<label class="error" for="guardian_mobile_number">:message</label>') !!}
    </div>
</div>
<hr>

<div class="row">
  <div class="col-md-8">
  </div>
  <div class="col-md-4 m-t-10 sm-m-t-10">
    {{ Form::submit(isset($student) ? 'Update Student' : 'Add Student', ['class' => 'btn btn-primary btn-block m-t-5']) }}
  </div>
</div>