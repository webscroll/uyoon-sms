@extends('layouts.admin')

@section('title', request()->input('q') ? 'Search Results' : 'Students')

@section('content')

@include('shared.notifications')

<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner" style="transform: translateY(0px); opacity: 1;">
      <!-- START BREADCRUMB -->
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('students.index') }}">Students</a></li>
        @if (request()->input('q'))
        <li class="breadcrumb-item active">Search Results</li>
        @endif
      </ol>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding    container-fixed-lg bg-white">
      <div class="container card card-default">
        <!-- START card -->
        <div class="card card-transparent">
          <div class="card-header ">
              <div class="row">
                  <div class="col-2 col-xs-12">
                        <a class="btn btn-primary btn-cons" href="{{ route('students.create') }}" >Add Student</a>
                  </div>
                  <div class="col-4 col-xs-12">
                        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control">
                    {{-- <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data" class="dropzone">
                        @csrf
                        <div class="fallback">
                            <input name="file" type="file" />
                        </div> --}}
                    </div>
                    <div class="col-4 col-xs-12">
                        <button class="btn btn-primary btn-cons">Import</button>
                        <a class="btn btn-primary btn-cons" href="{{ route('export') }}">Export</a>
                    </form>
                  </div>
                  <div class="col-2 col-xs-12">
                        <div class="pull-right">
                                <div class="col-xs-12">
                                  <form action="{{ route('students.index') }}">
                                      <div class="input-group transparent center-margin">
                                        <input type="text" class="form-control"  name="q" placeholder="Search" value="{{ request()->input('q') }}">
                                        <span class="input-group-addon ">
                                            <i class="pg-search"></i>
                                        </span>
                                      </div>
                                  </form>
                                </div>
                              </div>
                  </div>
              </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-block">

            <div class="table-responsive">
              <div  class="dataTables_wrapper no-footer">
                <table class="table table-hover table-condensed dataTable no-footer" role="grid">
                  <thead>
                    <tr role="row">
                      <th style="width: 15%">Student ID</th>
                      <th style="width: 30%">Name</th>
                      <th style="width: 15%">National ID</th>
                      <th style="width: 20%">Mobile Number</th>
                      <th style="width: 15%">gender</th>
                      <th style="width: 30%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($students as $student)
                    <tr>
                      <td class="v-align-middle">{{ $student->student_id }}</td>
                      <td class="v-align-middle semi-bold"><a href="{{ route('students.show', $student) }}">{{ $student->name }}</a></td>
                      <td class="v-align-middle">{{ $student->national_id }}</td>
                      <td class="v-align-middle semi-bold">{{ $student->mobile_number }}</td>
                      <td class="v-align-middle semi-bold">{{ $student->gender }}</td>
                      <td class="v-align-middle semi-bold">
                        <a href="{{ route('students.edit', $student) }}">Edit</a>
                        <div class="vertical-line"></div>
                        <form action="{{ route('students.destroy', ['id' => $student->id ]) }}?_method=DELETE" class="delete-onclick inline" method="POST">
                          @csrf
                          <button class="btn text-danger btn-link m-0 p-0" type="submit">Delete</button>
                          </button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

            {{-- Default Empty State --}}
            @component('shared.empty', ['route' => 'students.index', 'model' => 'students', 'data' => $students])
              {{-- Create new Bttn --}}
              <a class="btn btn-primary btn-cons" href="{{ route('students.create') }}">Add Student</a>
            @endcomponent

            <div class="mt-3">
                {{ $students->links() }}
            </div>
          </div>
        </div>
        <!-- END card -->
      </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection
