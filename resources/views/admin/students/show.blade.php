@extends('layouts.admin')

@section('title', 'Students')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
      <div class="col-lg-6 pull-right">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('students.index') }}">Students</a></li>
          <li class="breadcrumb-item active">{{ $student->name }}</li>
        </ol>
      </div>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
      <div class="row">
        <div class="container card card-default col-md-3 p-4 mr-0">
          <!-- START card -->
          <div class="d-flex flex-column full-height circular">
            <div class="thumbnail-wrapper circular mx-5">
                <img src="/img/user-large.png">
            </div>
            <h3 class="mb-0 text-center">{{ $student->name }}</h3>
            <p class="text-center mb-4">
              {{ $student->national_id}}, {{ $student->gender}}
            </p>
            <p class="align-baseline"><i data-feather="phone" class="mr-2"></i> {{ $student->mobile_number }}</p>
            <p class="align-baseline"><i data-feather="mail" class="mr-2"></i> {{ $student->email ?: '-' }}</p>
            <p class="align-baseline"><i data-feather="map-pin" class="mr-2"></i> {{ $student->permanent_address }}</p>
            <div class="b-b b-dashed b-grey my-2"></div>
            <p class="bold sm-p-t-20">Programs</p>
            @forelse ($student->batches as $batch)
            <div>
              <i data-feather="chevron-right" class="pull-left fs-14 m-t-5"></i>
              <p class="no-margin p-l-30">
                <a href="{{ route('batches.show', $batch) }}">{{ $batch->program->name }}</a>
              </p>
            </div>
            @empty
              -
            @endforelse
            <a href="{{ route('students.edit', $student) }}" class="btn btn-block mt-4">Edit Student</a>
          </div>
          <!-- END card -->
        </div>
        <div class="container card card-default col-md-8">
            <div class="col-md-12 mt-4">
              <div class="card card-transparent">
                <ul class="nav nav-tabs nav-tabs-linetriangle d-none d-md-flex d-lg-flex d-xl-flex" data-init-reponsive-tabs="dropdownfx">
                  <li class="nav-item">
                    <a href="#" class="" data-toggle="tab" data-target="#fade1"><span>Payments</span></a>
                  </li>
                  <li class="nav-item">
                    <a href="#" data-toggle="tab" data-target="#fade2" class=""><span>Documents</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="#" data-toggle="tab" data-target="#personal-infromation" class="active show"><span>Personal Details</span></a>
                    </li>
                </ul>
                <div class="nav-tab-dropdown cs-wrapper full-width d-lg-none d-xl-none d-md-none">
                  <div class="cs-select cs-skin-slide full-width" tabindex="0"><span class="cs-placeholder">Hello World</span>
                    <div class="cs-options">
                      <ul>
                        <li data-option="" data-value="#fade1"><span>Couse</span></li>
                        <li data-option="" data-value="#fade2"><span>Hello Two</span></li>
                      </ul>
                    </div><select class="cs-select cs-skin-slide full-width" data-init-plugin="cs-select"><option value="#fade1" selected="">Hello World</option><option value="#fade2">Hello Two</option><option value="#fade3">Hello Three</option></select>
                    <div
                      class="cs-backdrop"></div>
                </div>
              </div>

              <div class="tab-content">
                <div class="tab-pane fade " id="fade1">
                  <div class="row column-seperation">
                      <div class="">
                          <div class="table-responsive">
                            <table class="table table-hover table-condensed table-detailed" id="detailedTable">
                              <thead>
                                <tr>
                                  <th style="width:25%">Title</th>
                                  <th style="width:25%">Status</th>
                                  <th style="width:25%">Price</th>
                                  <th style="width:25%">Last Update</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td class="v-align-middle semi-bold">Revolution Begins</td>
                                  <td class="v-align-middle">Active</td>
                                  <td class="v-align-middle semi-bold">40,000 USD</td>
                                  <td class="v-align-middle">April 13, 2014</td>
                                </tr>
                                <tr>
                                  <td class="v-align-middle semi-bold">Revolution Begins</td>
                                  <td class="v-align-middle">Active</td>
                                  <td class="v-align-middle semi-bold">70,000 USD</td>
                                  <td class="v-align-middle">April 13, 2014</td>
                                </tr>
                                <tr>
                                  <td class="v-align-middle semi-bold">Revolution Begins</td>
                                  <td class="v-align-middle">Active</td>
                                  <td class="v-align-middle semi-bold">20,000 USD</td>
                                  <td class="v-align-middle">April 13, 2014</td>
                                </tr>
                                <tr>
                                  <td class="v-align-middle semi-bold">Revolution Begins</td>
                                  <td class="v-align-middle">Active</td>
                                  <td class="v-align-middle semi-bold">90,000 USD</td>
                                  <td class="v-align-middle">April 13, 2014</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="fade2">
                  <div class="row">
                    <div class="col-lg-12">
                      <h3>“ Nothing is
                        <span class="semi-bold">impossible</span>, the word itself says 'I'm
                        <span class="semi-bold">possible</span>'! ”
                      </h3>
                      <p>A style represents visual customizations on top of a layout. By editing a style, you can use Squarespace's visual
                        interface to customize your...</p>
                      <br>
                      <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-cons">Attach</button>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade active show" id="personal-infromation">
                    <div class="row">
                      <div class="col-lg-12">
                          <div class="table-responsive">
                              <h4  class="pull-left semi-bold ">Personal Details</h4>
                              <br>
                              <div class="row">
                                  <div class="col-6">
                                      <p class="pt-3 m-0">Basic Informations</p>
                                  </div>
                                  <div class="col-6 d-flex justify-content-end align-items-center">
                                      <a href="{{ route('students.edit', $student) }}" class="btn btn-primary btn-cons">Update</a>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>


                              <table class="table table-bordered">
                                <tbody>
                                <tr class="bg-light">
                                  <td class="success" width="30%">Student ID</td>
                                  <td>{{  $student->student_id }}</td>
                                </tr>
                                <tr>
                                  <td class="success">Name</td>
                                  <td>{{ $student->name }}</td>
                                </tr>
                                <tr>
                                  <td class="success">Date of Birth</td>
                                  <td>{{ date('jS F Y', strtotime($student->dob))}}</td>
                                </tr>
                                <tr>
                                  <td class="success">Nationality</td>
                                  <td>{{ $student->nationality}}</td>
                                </tr>
                                <tr>
                                  <td class="success">Highest Qualification</td>
                                  <td>{{ $student->level->name ??'' }}</td>
                                </tr>
                                </tbody>
                              </table>
                              <p class="pt-3 m-0">Contact Informations</p>
                              <table class="table table-bordered">
                                <tbody>
                                <tr class="bg-light">
                                  <td class="success" width="30%">Current Address</td>
                                  <td>{{  $student->current_address }}, {{  $student->current_island }}</td>
                                </tr>
                                <tr>
                                    <td class="success" width="30%">Permanent Address</td>
                                    <td>{{  $student->permanent_address }}, {{  $student->permanent_island }}</td>
                                </tr>
                                <tr>
                                  <td class="success">Mobile Number</td>
                                  <td>{{  $student->mobile_number }}</td>
                                </tr>
                                <tr>
                                  <td class="success">Email</td>
                                  <td>{{ $student->email ?: '-' }}</td>
                                </tr>
                                </tbody>
                              </table>
                              <p class="pt-3 m-0">Guardian Informations</p>
                              <table class="table table-bordered">
                                <tbody>
                                <tr class="bg-light">
                                  <td class="success" width="30%">Contact Name</td>
                                  <td>{{  $student->guardian_name ?: '-' }}</td>
                                </tr>
                                <tr>
                                    <td class="success" width="30%">Mobile Number</td>
                                    <td>{{  $student->guardian_mobile_number ?: '-' }}</td>
                                </tr>
                                </tbody>
                              </table>
                            </div>
                      </div>
                    </div>
                  </div>
              </div>

            </div>
            </div>

        </div>
      </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection
