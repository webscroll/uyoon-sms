<div class="row">
  <div class="col-md-8">
    <div class="form-group form-group-default required {{ $errors->has('short_code') ? 'has-error' :'' }}">
      {{ Form::label('Short Code') }}
      {{ Form::text('short_code', null, ['class' => 'form-control', 'placeholder' => 'Short Code', 'required',]) }}
    </div>
    {!! $errors->first('short_code', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-12">
      <div class="form-group form-group-default form-group-default-select2 required  {{ $errors->has('program_id') ? 'has-error' :'' }}">
          {{ Form::label('Programs') }}
          {{ Form::select('program_id', $programs->pluck('name', 'id'), null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'data-placeholder' => 'Select a program', 'placeholder' => 'Select a program']) }}
      </div>
    {!! $errors->first('program_id', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-group-default required {{ $errors->has('starts_at') ? 'has-error' :'' }}">
          {{ Form::label('Start Date') }}
          {{ Form::text('starts_at', null, ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'required', 'id' => 'datepicker-component', 'autocomplete' => 'off', 'type' => 'date']) }}
        </div>
        {!! $errors->first('starts_at', '<label class="error" for="starts_at">:message</label>') !!}
    </div>
    <div class="col-md-6">
        <div class="form-group form-group-default {{ $errors->has('ends_at') ? 'has-error' :'' }}">
          {{ Form::label('End Date') }}
          {{ Form::text('ends_at', null, ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'id' => 'datepicker-component', 'autocomplete' => 'off', 'type' => 'date']) }}
        </div>
        {!! $errors->first('ends_at', '<label class="error" for="ends_at">:message</label>') !!}
    </div>
  </div>

<div class="row clearfix">
  <div class="col-md-6">
      <div class="form-group form-group-default form-group-default-select2 required">
        {{ Form::label('Status') }}
        {{ Form::select('status', config('uyoon.programs.status'), null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'data-disable-search' => 'true']) }}
      </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="col-md-8">
  </div>
  <div class="col-md-4 m-t-10 sm-m-t-10">
    {{ Form::submit(isset($batch) ? 'Update Batch' : 'Add Batch', ['class' => 'btn btn-primary btn-block m-t-5']) }}
  </div>
</div>