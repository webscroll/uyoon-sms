@extends('layouts.admin')

@section('title', 'Batches')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
      <div class="col-lg-6 pull-right">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('batches.index') }}">Batches</a></li>
          <li class="breadcrumb-item active">{{ $batch->short_code }}</li>
        </ol>
      </div>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding container-fixed-lg bg-white">
      <div class="container card card-default">
        <!-- START card -->
        <div class="card card-transparent">
          <div class="card-header ">
            <h3 class=" no-margin">{{ $batch->short_code }}</h3>
            <div class="pull-right">
              <div class="col-xs-12">
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-block">
            <div class="row">
              <!-- START card -->
              <div class="card card-transparent">
                <div class="card-block">
                  <div class="row">
                    <div class="col-md-4">
                      <h5 class="semi-bold no-margin">Program</h5>
                      <h6 class="hint-text">{{ $batch->program->name }}</h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Level</h5>
                      <h6 class="hint-text">{{ $batch->program->level->type }} - {{ $batch->program->level->name }} </h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Total Students</h5>
                      <h6 class="hint-text">{{ $batch->program->total_credit }}</h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Status</h5>
                      <h6 class="hint-text">{{ $batch->status }}</h6>
                    </div>
                  </div>
                </div>
                <div class="card-block">
                  <div class="dropdown-divider"></div>
                </div>

                <div class="card-header pt-0">
                  <h4 class="pull-left semi-bold">Students</h4>
                  <div class="pull-right">
                      {{ Form::open(['route' => ['batches.students.store', $batch]]) }}
                        <div class="input-group form-group  form-group-default-select2">
                          {{ Form::select('student_id', $add_students->pluck('name', 'id'), null, ['data-init-plugin' => 'select2', 'data-placeholder' => 'Select a student', 'placeholder' => 'Select a student']) }}
                          {{ Form::submit('Add', ['class' => 'input-group-addon btn btn-primary px-3']) }}
                        </div>
                      {{ Form::close() }}
                  </div>
                </div>

                <div class="card-block">
                    <div class="table-responsive">
                      <div  class="dataTables_wrapper no-footer">
                        <table class="table table-hover table-condensed dataTable no-footer" role="grid">
                          <thead>
                            <tr role="row">
                              <th style="width: 15%">Student ID</th>
                              <th style="width: 30%">Name</th>
                              <th style="width: 15%">National ID</th>
                              <th style="width: 20%">Mobile Number</th>
                              <th style="width: 15%">Status</th>
                              <th style="width: 30%">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @forelse ($batch->students as $student)
                            <tr>
                              <td class="v-align-middle">S{{ $student->id }}</td>
                              <td class="v-align-middle semi-bold"><a href="{{ route('students.show', $student) }}">{{ $student->name }}</a></td>
                              <td class="v-align-middle">{{ $student->national_id }}</td>
                              <td class="v-align-middle semi-bold">{{ $student->mobile_number }}</td>
                              <td class="v-align-middle semi-bold">{{ $student->pivot->registration_status }}</td>
                              <td class="v-align-middle semi-bold">
                                <a href="{{ route('students.edit', $student) }}">Edit</a>
                                <div class="vertical-line"></div>
                                <form action="{{ route('batches.students.destroy', [$batch, $student]) }}?_method=DELETE" class="delete-onclick inline" method="POST">
                                  @csrf
                                  <button class="btn text-danger btn-link m-0 p-0" type="submit">Remove</button>
                                  </button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
        
                    {{-- Default Empty State --}}
                    @component('shared.empty', ['route' => 'students.index', 'model' => 'students', 'data' => $batch->students])
                      {{-- Create new Bttn --}}
                    @endcomponent
                  </div>

              </div>
              <!-- END card -->
            </div>


          </div>
        </div>
        <!-- END card -->
      </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection