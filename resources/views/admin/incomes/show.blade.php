@extends('layouts.admin')


@section('title', 'Incomes')


@section('content')
<!-- START JUMBOTRON -->
<div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
        <div class="col-lg-4 pull-right">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('incomes.index') }}">Income</a></li>
                <li class="breadcrumb-item active">Income Details</li>
            </ol>
        </div>
        <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
<div>
   <div class=" no-padding container-fixed-lg bg-white">
        <div class="container card card-default">
            <!-- START card -->
            <div class="card card-transparent">
                <div class="card-header ">         
                    <div class="row">
                            <div class="col-md-8">
                                <h4>Income Details</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="pull-right">
                                    <h5 >Invoice Number: {{ $income->invoice_number}}</h5>                        
                                  </div>
                            </div>
                          </div>
                </div>
                <div class="card-block">
                    <form id="form-personal" role="form" novalidate="novalidate" action="" method="POST">
                        <!-- START card -->
                        <div class="card card-transparent">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Title</h5>
                                        <h6 class="">{{ $income->title}}</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <h5 class="semi-bold no-margin">Type</h5>
                                        <h6 class="">{{ $income->type }}</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <h5 class="semi-bold no-margin">Description</h5>
                                        <h6 class="">{{ $income->description}}</h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Date</h5>
                                        <h6 class="">{{ $income->date }}</h6>
                                    </div>                                    
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Cheque Number</h5>
                                        <h6 class="">{{ $income->cheque_number}}</h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Received by</h5>
                                        <h6 class="">{{ $income->received_by}}</h5>
                                    </div>                                    
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Created at</h5>
                                        <h6 class="">{{ $income->created_at }}</h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Last Updated at</h5>
                                        <h6 class="">{{ $income->updated_at }}</h6>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <!-- END card -->
        </div>
    </div>
    
</div>
<div>
   <div class=" no-padding container-fixed-lg bg-white">
        <div class="container card card-default">
            <!-- START card -->
            <div class="card card-transparent">
                <div class="card-header ">
                  <div class="row">
                    <div class="col-md-8">
                        <h4>Customer Details</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right">
                            <h5 >Customer ID: {{ $income->customer->c_id}}</h5>                        
                          </div>
                    </div>
                  </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-block">
                    <form id="form-personal" role="form" novalidate="novalidate" action="" method="POST">
                        <!-- START card -->
                        <div class="card card-transparent">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Name</h5>
                                        <h6 class="">{{ $income->customer->name}}</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5 class="semi-bold no-margin">Aaddress</h5>
                                        <h6 class="">{{ $income->customer->address }}</h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h5 class="semi-bold no-margin">Email</h5>
                                        <h6 class="">{{ $income->customer->email}}</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <h5 class="semi-bold no-margin">Mobile Number</h5>
                                        <h6 class="">{{ $income->customer->mobile_number }}</h6>
                                    </div>                                  
                                </div>                              
                            </div>
                    </form>
                </div>
            </div>
            <!-- END card -->
        </div>
    </div>
    </div>
    
</div>


<!-- END PAGE CONTENT -->
</div>
@endsection
