<div class="row clearfix">
    <div class="col-md-6">
        <div class="form-group form-group-default form-group-default-select2 required">
            <label class="">Customer</label>
            <select class="full-width" data-placeholder="Select Customer" data-init-plugin="select2" name="customer_id">
                @foreach ($customers as $customer)
                <option value="{{ $customer->id }}"
                    {{ isset($income) ? ($customer->id == $income->customer_id ? 'selected' : '') : '' }}>{{
                    $customer->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group form-group-default required {{ $errors->has('title') ? 'has-error' :'' }}">
            {{ Form::label('Title') }}
            {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title', 'required',]) }}
        </div>
        {!! $errors->first('title', '<label class="error" for="name">:message</label>') !!}
    </div>

    <div class="col-md-12">
        <div class="form-group form-group-default required {{ $errors->has('description') ? 'has-error' :'' }}">
            {{ Form::label('Description') }}
            {{ Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description',
            'required',]) }}
        </div>
        {!! $errors->first('description', '<label class="error" for="name">:message</label>') !!}
    </div>
    <div class="col-md-6">
        <div class="form-group form-group-default form-group-default-select2 required">
          {{ Form::label('Type') }}
          {{ Form::select('type', ['Cash' => 'Cash', 'Cheque' => 'Cheque'], null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'data-disable-search' => 'true']) }}
        </div>
    </div>

    {{-- <div class="col-md-6">
            <div class="form-group form-group-default form-group-default-select2 required">
              <label class="">Type</label>
              <select class="full-width" data-placeholder="Select Type" data-init-plugin="select2" name="type"> 
                    <option value=""></option>
                    <option value="Cash">Cash</option>
                    <option value="Cheque">Cheque</option>
              </select>
            </div>
      </div> --}}
    {{-- <div class="col-md-6">
        <div class="form-group form-group-default required {{ $errors->has('type') ? 'has-error' :'' }}">
            {{ Form::label('Type') }}
            {{ Form::text('type', null, ['class' => 'form-control', 'placeholder' => 'Type', 'required',]) }}
        </div>
        {!! $errors->first('type', '<label class="error" for="name">:message</label>') !!}
    </div> --}}
    <div class="col-md-6 clearfix">
        <div class="form-group form-group-default  {{ $errors->has('cheque_number') ? 'has-error' :'' }}">
            {{ Form::label('Cheque Number') }}
            {{ Form::text('cheque_number', null, ['class' => 'form-control', 'placeholder' => 'Cheque Number', ]) }}
        </div>
        {!! $errors->first('cheque_number', '<label class="error" for="name">:message</label>') !!}
    </div>

    
    <div class="col-md-6 clearfix">
        <div class="form-group form-group-default required {{ $errors->has('date') ? 'has-error' :'' }}">
            {{ Form::label('Date') }}
            {{ Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'Date', 'required', 'id' =>
            'datepicker-component2', 'data-date-format' => 'yyyy-mm-dd',]) }}
        </div>
        {!! $errors->first('date', '<label class="error" for="name">:message</label>') !!}
    </div>

    <div class="col-md-6">
        <div class="form-group form-group-default required {{ $errors->has('amount') ? 'has-error' :'' }}">
          {{ Form::label('Amount') }}
          {{ Form::number('amount', null, ['class' => 'form-control', 'placeholder' => 'Amount', 'required',]) }}
          {{ Form::hidden('received_by',Auth()->user()->name, ['class' => 'form-control',]) }}
        </div>
        {!! $errors->first('amount', '<label class="error" for="name">:message</label>') !!}
      </div>
    {{-- <div class="col-md-12 clearfix">
        <div class="form-group form-group-default required {{ $errors->has('invoice_number') ? 'has-error' :'' }}">
            {{ Form::label('Invoice Number') }}
            {{ Form::text('invoice_number', null, ['class' => 'form-control', 'placeholder' => 'Invoice Number',
            'required',]) }}
        </div>
        {!! $errors->first('invoice_number', '<label class="error" for="name">:message</label>') !!}
    </div> --}}


</div>
<hr>
<div class="row">
    <div class="col-md-8">
    </div>
    <div class="col-md-4 m-t-10 sm-m-t-10">
        {{ Form::submit(isset($income) ? 'Update Income' : 'Add Income', ['class' => 'btn btn-primary btn-block
        m-t-5']) }}
    </div>
</div>
