<div class="mt-auto">
  <div class="bg-master-light padding-20 full-width">
      {{ Form::open(['route' => 'events.store', 'id' => 'create-event']) }}
        <div class="row">
          <div class="col-10">
            <p class="no-margin normal text-black">Type Event Here</p>
              <div class="input-group transparent no-border full-width">
                  {{ Form::hidden('date',  date('y-m-d')) }}
                  {{ Form::text('title', null, ['class' => 'form-control transparent pl-0', 'placeholder' => 'What do you need to remeber?', 'autocomplete' => 'off']) }}
              </div>
          </div>
          <div class="col-2 text-center">
          {{ Form::button('<div class="block m-t-15"><img src="/img/plus.svg"></div>', ['class' => 'btn btn-link pl-0', 'type' => 'submit']) }}
          </div>
        </div>
      {{ Form::close() }}
  </div>
</div>