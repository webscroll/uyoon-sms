<ul class="list-unstyled p-l-20 p-r-20 p-t-30 m-b-20">
    <li>
        <h4 class="pull-left normal no-margin"> Todo List</h4>
        <a href="#" class="text-black pull-right m-r-5" data-toggle="refresh"><i class="card-icon card-icon-refresh"></i></a>
    </li>
    <div class="clearfix"></div>
</ul>
<div class="widget-11-2">
        <div class="auto-overflow widget-11-2-table">
            <div class="task-list p-t-0 p-r-20 p-b-20 p-l-20 clearfix flex-1 event-list auto-overflow">

            </div>
        </div>
    </div>
<div class="clearfix"></div>

@include('admin.events.create')