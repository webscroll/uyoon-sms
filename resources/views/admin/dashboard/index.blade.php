@extends('layouts.admin') 
@section('title', 'Dashboard') 
@section('content')

<!-- START PAGE CONTENT -->
<div class="content sm-gutter">
  <!-- START JUMBOTRON -->
  <div data-pages="parallax">
    <div class=" container no-padding    container-fixed-lg">
      <div class="inner">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- END JUMBOTRON -->

  <!-- START CONTAINER FLUID -->
  <div class="container sm-padding-10 no-padding">
    <!-- START ROW -->
    <div class="row">
      <div class="col-lg-3 col-sm-6 d-flex flex-column">
        <!-- START WIDGET widget_weekly_sales_card-->
        <div class="card no-border widget-loader-bar m-b-10">
          <div class="container-xs-height full-height">
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="card-header  top-left top-right">
                  <div class="card-title">
                    <h5 class="hint-text no-margin">Students</h5>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="p-l-20 p-t-50 p-b-30 p-r-20">
                  <h3 class="m-b-0">{{ $students_count }}
                  </h3>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-bottom">
                <div class="progress progress-small m-b-0">
                  <!-- START BOOTSTRAP PROGRESS -->
                  <div class="progress-bar" style="width:%"></div>
                  <!-- END BOOTSTRAP PROGRESS -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END WIDGET -->
      </div>

      <div class="col-lg-3 col-sm-6 d-flex flex-column">
        <!-- START WIDGET widget_weekly_sales_card-->
        <div class="card no-border widget-loader-bar m-b-10">
          <div class="container-xs-height full-height">
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="card-header  top-left top-right">
                  <div class="card-title">
                    <h5 class="hint-text no-margin">Batches</h5>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="p-l-20 p-t-50 p-b-30 p-r-20">
                  <h3 class="m-b-0">{{ $batches_count }}<span class="pl-2"><i class="fa"></i></span>
                  </h3>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-bottom">
                <div class="progress progress-small m-b-0">
                  <!-- START BOOTSTRAP PROGRESS -->
                  <div class="progress-bar" style="width:%"></div>
                  <!-- END BOOTSTRAP PROGRESS -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END WIDGET -->
      </div>


      <div class="col-lg-3 col-sm-6 d-flex flex-column">
        <!-- START WIDGET widget_weekly_sales_card-->
        <div class="card no-border widget-loader-bar m-b-10">
          <div class="container-xs-height full-height">
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="card-header  top-left top-right">
                  <div class="card-title">
                    <h5 class="hint-text no-margin">Income</h5>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="p-l-20 p-t-50 p-b-30 p-r-20">
                  <h3 class="m-b-0">{{ $programs_count }}<span class="pl-2 "><i class="fa "></i> </span>
                  </h3>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-bottom">
                <div class="progress progress-small m-b-0">
                  <!-- START BOOTSTRAP PROGRESS -->
                  <div class="progress-bar " style="width:%"></div>
                  <!-- END BOOTSTRAP PROGRESS -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END WIDGET -->
      </div>

      <div class="col-lg-3 col-sm-6 d-flex flex-column">
        <!-- START WIDGET widget_weekly_sales_card-->
        <div class="card no-border widget-loader-bar m-b-10">
          <div class="container-xs-height full-height">
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="card-header  top-left top-right">
                  <div class="card-title">
                    <h5 class="hint-text no-margin">Expenses</h5>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-top">
                <div class="p-l-20 p-t-50 p-b-30 p-r-20">
                    <h3 class="m-b-0">{{ $expenses_count }}<span class="pl-2 "><i class="fa "></i></span>
                  </h3>
                </div>
              </div>
            </div>
            <div class="row-xs-height">
              <div class="col-xs-height col-bottom">
                <div class="progress progress-small m-b-0">
                  <!-- START BOOTSTRAP PROGRESS -->
                  <div class="progress-bar"></div>
                  <!-- END BOOTSTRAP PROGRESS -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END WIDGET -->
      </div>
    </div>
    <!-- END ROW -->

    <!-- START ROW -->
    <div class="row m-b-30">
      <div class="col-lg-7">
        <!-- START ROW -->
        <div class="row md-m-b-10">
          <div class="col-sm-6">
            <!-- START WIDGET widget_pendingComments.tpl-->
            <div class=" card no-border  no-margin widget-loader-circle todolist-widget pending-projects-widget">
              <div class="card-header ">
                <div class="card-title">
                  <span class="font-montserrat fs-11 all-caps">
                        Recent Applications <i class="fa fa-chevron-right"></i>
                    </span>
                </div>
                <div class="card-controls">
                  <ul>
                    <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i class="card-icon card-icon-refresh"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="card-block">
                <h5 class="">Online applicants<span class="semi-bold"> received</span></h5>
                <ul class="nav nav-tabs nav-tabs-simple m-b-20" role="tablist" data-init-reponsive-tabs="collapse">
                  <li class="nav-item"><a href="#pending" class="active" data-toggle="tab" role="tab" aria-expanded="true">Pending</a>
                  </li>
                  <li class="nav-item"><a href="#approved" data-toggle="tab" role="tab" aria-expanded="false">Approved</a>
                  </li>
                </ul>
                <div class="tab-content no-padding">
                  <div class="tab-pane active" id="pending">
                    <div class="p-t-15">
                      <div class="d-flex">
                        <span class="icon-thumbnail bg-master-light pull-left text-master">ws</span>
                        <div class="flex-1 full-width overflow-ellipsis">
                          <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Revox Ltd
                          </p>
                          <h5 class="no-margin overflow-ellipsis ">Marketing Campaign for revox</h5>
                        </div>
                      </div>
                    </div>
                    <div class="p-t-15">
                      <div class="d-flex">
                        <span class="icon-thumbnail bg-warning-light pull-left text-master">cr</span>
                        <div class="flex-1 full-width overflow-ellipsis">
                          <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Nike Ltd
                          </p>
                          <h5 class="no-margin overflow-ellipsis ">Corporate rebranding</h5>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="btn btn-block m-t-30">See all Applicants</a>
                  </div>
                  <div class="tab-pane" id="approved">
                    <div class="p-t-15">
                      <div class="d-flex">
                        <span class="icon-thumbnail bg-master-light pull-left text-master">ws</span>
                        <div class="flex-1 full-width overflow-ellipsis">
                          <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Apple Corp
                          </p>
                          <h5 class="no-margin overflow-ellipsis ">Marketing Campaign for revox</h5>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="m-t-15">
                        <p class="hint-text fade small pull-left">45% compleated from total</p>
                        <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                        <div class="clearfix"></div>
                      </div>
                      <div class="progress progress-small m-b-15 m-t-10">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-info" style="width:45%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                      </div>
                    </div>
                    <div class="p-t-15">
                      <div class="d-flex">
                        <span class="icon-thumbnail bg-warning-light pull-left text-master">cr</span>
                        <div class="flex-1 full-width overflow-ellipsis">
                          <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Yahoo Inc
                          </p>
                          <h5 class="no-margin overflow-ellipsis ">Corporate rebranding</h5>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="m-t-25">
                        <p class="hint-text fade small pull-left">20% compleated from total</p>
                        <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                        <div class="clearfix"></div>
                      </div>
                      <div class="progress progress-small m-b-15 m-t-10">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-warning" style="width:20%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                      </div>
                    </div>
                    <a href="#" class="btn btn-block m-t-30">See all Applicants</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- END WIDGET -->
          </div>
          <div class="col-sm-6 sm-m-t-10 d-flex align-items-stretch">
            <!-- START WIDGET widget_pendingComments.tpl-->
            <div class=" widget-11-2 card no-border  no-margin widget-loader-circle todolist-widget align-self-stretch">
                @include('admin.events.index')
            </div>
            <!-- END WIDGET -->
          </div>
        </div>
        <!-- END ROW -->
      </div>
      <div class="col-lg-5 sm-m-t-10 d-flex align-items-stretch">
        <!-- START WIDGET widget_pendingComments.tpl-->
        <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle d-flex flex-column align-self-stretch">
          <div class="card-header top-right">
            <div class="card-controls">
              <ul>
                <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                </li>
              </ul>
            </div>
          </div>
          <div class="padding-25">
            <div class="pull-left">
              <h2 class="text-complete no-margin">Income</h2>
              <p class="no-margin">Today's sales</p>
            </div>
            <h3 class="pull-right semi-bold"><sup>
                <small class="semi-bold">MVR</small>
            </sup> {{ number_format($incomes_count, 0, '.', ',') }}
            </h3>
            <div class="clearfix"></div>
          </div>
          <div class="auto-overflow widget-11-2-table">
            <table class="table table-condensed table-hover">
              <tbody>
                <tr>
                  @forelse ($incomes as $income)
                  <td class="font-montserrat all-caps fs-12 w-50">{{ $income->title }}</td>
                  <td class="text-right b-r b-dashed b-grey w-25">
                    <span class="hint-text small">Inv.#{{ $income->invoice_number }}</span>
                  </td>
                  <td class="w-25">
                    <span class="font-montserrat fs-18">MVR {{ $income->amount }}</span>
                  </td>
                  @empty
                  <div class="text-center pt-5">
                      <img src="/img/empty.svg" class="img-fluid" alt="Responsive image">
                  </div>
                  <div class="text-center mt-3">
                      You don't have any incomes yet.
                  </div>
                  @endforelse
                </tr>
              </tbody>
            </table>
          </div>
          <div class="padding-25 mt-auto">
            <p class="small no-margin">
              <a href="{{ route('incomes.index') }}"><i class="fa fs-16 fa-arrow-circle-o-down text-complete m-r-10"></i></a>
              <span class="hint-text">Show more Incomes</span>
            </p>
          </div>
        </div>
        <!-- END WIDGET -->
      </div>
    </div>
    <!-- END ROW -->
  </div>
  <!-- END CONTAINER FLUID -->
</div>
@endsection