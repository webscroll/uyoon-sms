<div class="row">
  <div class="col-md-8">
    <div class="form-group form-group-default required {{ $errors->has('short_code') ? 'has-error' :'' }}">
      {{ Form::label('Short Code') }}
      {{ Form::text('short_code', null, ['class' => 'form-control', 'placeholder' => 'Short Code', 'required',]) }}
    </div>
    {!! $errors->first('short_code', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-12">
    <div class="form-group form-group-default required {{ $errors->has('name') ? 'has-error' :'' }}">
      {{ Form::label('Program name') }}
      {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Program Name', 'required']) }}
    </div>
    {!! $errors->first('name', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-6">
    <div class="form-group form-group-default form-group-default-select2 required">
      <label class="">Program Level</label>
      <select class="full-width" data-placeholder="Select Level" data-init-plugin="select2" name="program_level_id">
        @foreach ($levels as $type => $levelGrouped)
          <optgroup label="{{ $type }}">
            @foreach ($levelGrouped as $level)
              <option value="{{ $level->id }}" {{ isset($program) ? ($program->program_level_id == $level->id ? 'selected' : '') : '' }}>{{ $level->name }}</option>
            @endforeach
          </optgroup>
        @endforeach
      </select>
    </div>
  </div>

  <div class="col-md-6">
      <div class="form-group form-group-default form-group-default-select2 required">
        {{ Form::label('Status') }}
        {{ Form::select('status', config('uyoon.programs.status'), null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'data-disable-search' => 'true']) }}
      </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="col-md-8">
  </div>
  <div class="col-md-4 m-t-10 sm-m-t-10">
    {{ Form::submit(isset($program) ? 'Update Program' : 'Add Program', ['class' => 'btn btn-primary btn-block m-t-5']) }}
  </div>
</div>