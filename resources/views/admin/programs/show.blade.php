@extends('layouts.admin')

@section('title', 'Programs')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
      <div class="col-lg-6 pull-right">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('programs.index') }}">Programs</a></li>
          <li class="breadcrumb-item active">{{ $program->name }}</li>
        </ol>
      </div>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding container-fixed-lg bg-white">
      <div class="container card card-default">
        <!-- START card -->
        <div class="card card-transparent">
          <div class="card-header ">
            <h3 class=" no-margin">{{ $program->name }}</h3>
            <div class="pull-right">
              <div class="col-xs-12">
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-block">
            <div class="row">
              <!-- START card -->
              <div class="card card-transparent">
                <div class="card-block">
                  <div class="row">
                    <div class="col-md-2">
                      <h5 class="semi-bold no-margin">Code</h5>
                      <h6 class="hint-text">{{ $program->short_code}}</h6>
                    </div>
                    <div class="col-md-4">
                      <h5 class="semi-bold no-margin">Program</h5>
                      <h6 class="hint-text">{{ $program->name }}</h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Level</h5>
                      <h6 class="hint-text">{{ $program->level->name }} - {{ $program->level->type}} </h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Credits</h5>
                      <h6 class="hint-text">{{ $program->total_credit }}</h6>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Status</h5>
                      <h6 class="hint-text">{{ $program->status }}</h6>
                    </div>
                  </div>
                </div>
                <div class="card-block">
                  <div class="dropdown-divider"></div>
                </div>

                <div class="card-header pt-0">
                  <h4 class="pull-left semi-bold">Modules</h4>
                  <div class="pull-right">
                      {{ Form::open(['route' => ['programs.modules.store', $program]]) }}
                        <div class="input-group form-group  form-group-default-select2">
                          {{ Form::select('module_id', $add_modules->pluck('name', 'id'), null, ['data-init-plugin' => 'select2', 'data-placeholder' => 'Select a module', 'placeholder' => 'Select a module ...']) }}
                          {{ Form::submit('Add', ['class' => 'input-group-addon btn btn-primary px-3']) }}
                        </div>
                      {{ Form::close() }}
                  </div>
                </div>

                <div class="card-block">
                    <div class="table-responsive">
                      <div  class="dataTables_wrapper no-footer">
                        <table class="table table-hover table-condensed dataTable no-footer" role="grid">
                          <thead>
                            <tr role="row">
                              <th style="width: 10%">Code</th>
                              <th style="width: 40%">Name</th>
                              <th style="width: 20%">Credit</th>
                              <th style="width: 20%">Fees</th>
                              <th style="width: 20%">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @forelse ($program->modules as $module)
                            <tr>
                              <td class="v-align-middle">{{ $module->short_code }}</td>
                              <td class="v-align-middle semi-bold"><a href="{{ route('modules.show', $module) }}">{{ $module->name }}</a></td>
                              <td class="v-align-middle semi-bold">{{ $module->credit }}</td>
                              <td class="v-align-middle semi-bold">MVR {{ $module->fees }}</td>
                              <td class="v-align-middle semi-bold">
                                <form action="{{ route('programs.modules.destroy', [$program, $module]) }}?_method=DELETE" class="delete-onclick inline" method="POST">
                                  @csrf
                                  <button class="btn text-danger btn-link m-0 p-0" type="submit">Remove</button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
        
                    {{-- Default Empty State --}}
                    @component('shared.empty', ['route' => 'modules.index', 'model' => 'modules', 'data' => $program->modules])
                      {{-- Create new Bttn --}}
                    @endcomponent
                  </div>

              </div>
              <!-- END card -->
            </div>


          </div>
        </div>
        <!-- END card -->
      </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection