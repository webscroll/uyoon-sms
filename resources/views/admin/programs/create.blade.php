<!-- Modal -->
<div class="modal fade slide-up" id="create-program" tabindex="-1" role="dialog" aria-hidden="false">
  <div class="modal-dialog ">
    <div class="modal-content-wrapper">
      <div class="modal-content">
        <div class="modal-header clearfix text-left">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
          <h5><span class="semi-bold">Add Program</span></h5>
          <p class="p-b-10">Please make sure all the required information is filled</p>
        </div>
        
        <div class="modal-body">
          {{ Form::open(['route' => 'programs.store', 'novalidate' => 'novalidate', 'role' => 'form', 'id' => 'form-personal']) }}
            @include('admin.programs.form') 
          {{ Form::close() }}
        </div>

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
</div>