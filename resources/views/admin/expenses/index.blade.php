@extends('layouts.admin')

@section('title', 'Expenses')

@section('content')

@include('admin.expenses.create')
@include('shared.notifications')

<!-- START PAGE CONTENT -->
<div class="content ">
    <!-- START JUMBOTRON -->
    <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner" style="transform: translateY(0px); opacity: 1;">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Expenses</li>
            </ol>
            <!-- END BREADCRUMB -->
        </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding    container-fixed-lg bg-white">
        <div class="container card card-default">
            <!-- START card -->
            <div class="card card-transparent">
                <div class="card-header ">
                    <a class="btn btn-primary btn-cons" href="#" data-target="#create-expense" data-toggle="modal"></i>
                        Add Expense</a>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <form action="{{ route('expenses.index') }}">
                                    <div class="input-group transparent center-margin">
                                        <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="{{ request()->input('q') }}">
                                        <span class="input-group-addon ">
                                            <i class="pg-search"></i>
                                        </span>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-block">                  

                    <div class="table-responsive">
                        <div class="dataTables_wrapper no-footer">
                            <table class="table table-hover table-condensed dataTable no-footer" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th style="width: 15%">Vendor Name</th>
                                        <th style="width: 20%">Title</th>
                                        <th style="width: 15%">Purchase Voucher</th>
                                        <th style="width: 10%">Type</th>
                                        <th style="width: 10%">Amount</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    @foreach ($expenses as $expense)
                                    <tr>                                        
                                        <td class="v-align-middle semi-bold">{{ $expense->vendor->name }}</td>
                                        <td class="sorting v-align-middle semi-bold"><a href="{{ route('expenses.show', $expense) }}">{{
                                            $expense->title }}</a></td>
                                        <td class="v-align-middle semi-bold">{{ $expense->pv_number }}</td>
                                        <td class="v-align-middle semi-bold">{{ $expense->type }}</td>
                                        <td class="v-align-middle semi-bold">{{ $expense->amount }}</td>
                                        <td class="v-align-middle semi-bold">
                                            <a href="{{ route('expenses.edit', $expense) }}">Edit</a>
                                            <div class="vertical-line"></div>
                                            <form action="{{ route('expenses.destroy', ['id' => $expense->id ]) }}?_method=DELETE"
                                                class="delete-onclick inline" method="POST">
                                                @csrf
                                                <button class="btn text-danger btn-link m-0 p-0" type="submit">Delete</button>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    {{-- Default Empty State --}}
                    @component('shared.empty', ['route' => 'expenses.index', 'model' => 'expenses', 'data' =>
                    $expenses])
                    {{-- Create new Bttn --}}
                    <a class="btn btn-primary btn-cons" href="#" data-target="#create-expense" data-toggle="modal">Add
                        Expense</a>
                    @endcomponent

                    <div class="mt-3">
                        {{ $expenses->links() }}
                    </div>
                </div>
            </div>
            <!-- END card -->           
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
    </div>
    <!-- END PAGE CONTENT -->
</div>
@endsection
