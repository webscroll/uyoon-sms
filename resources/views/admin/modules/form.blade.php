<p class="m-t-10">Module Information</p>

<div class="row">
  <div class="col-md-8">
    <div class="form-group form-group-default required {{ $errors->has('short_code') ? 'has-error' :'' }}">
      {{ Form::label('Short Code') }}
      {{ Form::text('short_code', null, ['class' => 'form-control', 'placeholder' => 'Short Code', 'required',]) }}
    </div>
    {!! $errors->first('short_code', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-12">
    <div class="form-group form-group-default required {{ $errors->has('name') ? 'has-error' :'' }}">
      {{ Form::label('Module name') }}
      {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Module Name', 'required']) }}
    </div>
    {!! $errors->first('name', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<p class="m-t-10">Module Specification</p>
<div class="row clearfix">
  <div class="col-md-8">
    <div class="form-group form-group-default required {{ $errors->has('credit') ? 'has-error' :'' }}">
      {{ Form::label('Credit') }}
      {{ Form::number('credit', null, ['class' => 'form-control', 'placeholder' => 'Credit', 'required']) }}
    </div>
    {!! $errors->first('credit', '<label class="error" for="credit">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-6">
    <div class="form-group form-group-default input-group required {{ $errors->has('fees') ? 'has-error' :'' }}">
        <div class="input-group-addon">
            MVR
        </div>
        <div class="form-input-group">
            {{ Form::label('Fees') }}
            {{ Form::number('fees', null, ['class' => 'form-control', 'placeholder' => 'Fees', 'required']) }}
        </div>

      </div>
    {!! $errors->first('fees', '<label class="error" for="name">:message</label>') !!}
  </div>

  <div class="col-md-6">
      <div class="form-group form-group-default input-group required {{ $errors->has('repeat_fees') ? 'has-error' :'' }}">
          <div class="input-group-addon">
              MVR
          </div>
          <div class="form-input-group">
              {{ Form::label('Repeat Fees') }}
              {{ Form::number('repeat_fees', null, ['class' => 'form-control', 'placeholder' => 'Repeat Fees', 'required']) }}
          </div>
        </div>
    {!! $errors->first('repeat_fees', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<div class="row clearfix">
  <div class="col-md-6">
    <div class="form-group form-group-default input-group {{ $errors->has('learning_hours') ? 'has-error' :'' }}">
      <div class="form-input-group">
          {{ Form::label('Learning Hours') }}
          {{ Form::number('learning_hours', null, ['class' => 'form-control', 'placeholder' => 'Learning Hours']) }}
      </div>
      <div class="input-group-addon">
        Hours
      </div>
    </div>
    {!! $errors->first('learning_hours', '<label class="error" for="name">:message</label>') !!}
  </div>

  <div class="col-md-6">
    <div class="form-group form-group-default input-group {{ $errors->has('contact_hours') ? 'has-error' :'' }}">
        <div class="form-input-group">
            {{ Form::label('Contact Hours') }}
            {{ Form::number('contact_hours', null, ['class' => 'form-control', 'placeholder' => 'Contact Hours']) }}
        </div>
        <div class="input-group-addon">
          Hours
        </div>
      </div>
     {!! $errors->first('contact_hours', '<label class="error" for="name">:message</label>') !!}
  </div>
</div>

<hr>

<div class="row">
  <div class="col-md-8">

  </div>
  <div class="col-md-4 m-t-10 sm-m-t-10">
    {{ Form::submit(isset($module) ? 'Update Module' : 'Add Module', ['class' => 'btn btn-primary btn-block m-t-5']) }}
  </div>
</div>