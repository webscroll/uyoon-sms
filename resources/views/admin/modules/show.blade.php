@extends('layouts.admin')

@section('content')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START JUMBOTRON -->
  <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="row">
      <div class="col-lg-6 pull-right">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('programs.index') }}">Programs</a></li>
          <li class="breadcrumb-item active">{{ $program->name }}</li>
        </ol>
      </div>
      <!-- END BREADCRUMB -->
    </div>

    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding container-fixed-lg bg-white">
      <div class="container card card-default">
        <!-- START card -->
        <div class="card card-transparent">
          <div class="card-header ">
            <div class="card-title">Programs</div>
            <h3 class="text-primary no-margin">{{ $program->name }}</h3>
            <div class="pull-right">
              <div class="col-xs-12">
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="card-block">

            <div class="row">
              <!-- START card -->
              <div class="card card-transparent">
                <div class="card-block">
                  <div class="row">
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Code</h5>
                      <p class="hint-text">{{ $program->short_code}}</p>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Program</h5>
                      <p class="hint-text">{{ $program->name }}</p>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Level</h5>
                      <p class="hint-text">{{ $program->level->name }} - {{ $program->level->type}} </p>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Total Credits</h5>
                      <p class="hint-text">{{ $program->total_credit }}</p>
                    </div>
                    <div class="col-md">
                      <h5 class="semi-bold no-margin">Status</h5>
                      <p class="hint-text">{{ $program->status }}</p>
                    </div>
                  </div>
                </div>

                <div class="card-block">
                  <div class="table-responsive">
                    <div id="condensedTable_wrapper" class="dataTables_wrapper no-footer">
                      <table class="table table-hover table-condensed dataTable no-footer" id="condensedTable" role="grid">
                        <thead>
                          <tr role="row">
                            <th style="width:1%" class="text-center sorting_disabled" rowspan="1" colspan="1"
                              aria-label="">
                              <button class="btn btn-link"><i class="pg-trash"></i>
                              </button>
                            </th>
                            <th style="width: 40%">Name</th>
                            <th style="width: 20%">Code</th>
                            <th style="width: 10%">Credit</th>
                            <th style="width: 20%">Status</th>
                            <th style="width: 30%">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {{-- @foreach ($programs as $program)
                          <tr>
                            <td class="v-align-middle semi-bold"><input type="checkbox" value="3" id="checkbox1"><label
                                for="checkbox1" class="no-padding no-margin"></label></td>
                            <td class="v-align-middle semi-bold">{{ $program->name }}</td>
                            <td class="v-align-middle">{{ $program->short_code }}</td>
                            <td class="v-align-middle semi-bold">{{ $program->total_credit }}</td>
                            <td class="v-align-middle semi-bold">{{ $program->status }}</td>
                            <td class="v-align-middle semi-bold">
                              <a class="btn btn-defaultr btn-xs" href="{{ route('programs.show', ['id' => $program->id ]) }}"><i
                                  class="fa fa-eye"></i>
                                <span class="bold">View Program</span>
                              </a>
                              <a class="btn btn-default btn-xs" href="{{ route('programs.edit', ['id' => $program->id ]) }}"><i
                                  class="fa  fa-edit"></i>
                                <span class="bold">Edit</span>
                              </a>
                              <form action="?_method=DELETE" class="delete-onclick inline" method="POST">
                                @csrf
                                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o"></i>
                                  <span class="bold">Delete</span>
                                </button>
                              </form>
                            </td>
                          </tr>
                          @endforeach --}}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END card -->
            </div>


          </div>
        </div>
        <!-- END card -->
      </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <!-- START CONTAINER FLUID -->
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->
</div>
@endsection